#!/usr/bin/perl

#
# gigdump2ahs.pl - Creates a .ahs instr. definition script from gigdump's output
#
# Copyright (C) 2005	  Angel Ortega <angel@triptico.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# http://www.triptico.com
#

$VERSION = "1.0";

$num = 1;

my @ins = ();
my %ins = ();

my $last_seen = '';

my $offset = 0;

while(<>)
{
	chomp;

	if(my ($sample, $start, $end) =
			(/"([^"]+)".*Start=(\d+), End=(\d+),/))
	{
		my $i;

		$i->{sample} = $sample;
		$i->{filename} = "${num}_${sample}.wav";
		$i->{base} = 0;
		$i->{min} = 0;
		$i->{max} = 0;
		$i->{start} = $start;
		$i->{end} = $end;

		push(@ins, $sample);
		$ins{$sample} = $i;

		$num ++;
	}
	elsif(my ($sample) = (/^\s+Sample \d+\) "(.+)"/))
	{
		my $i;

		$i->{sample} = $sample;
		$i->{filename} = "${num}_${sample}.wav";
		$i->{base} = 0;
		$i->{min} = 0;
		$i->{max} = 0;

		push(@ins, $sample);
		$ins{$sample} = $i;

		$num ++;
	}
	elsif(/^\s+Region \d+\) Sample: "(.+)"/)
	{
		$last_seen = $1;
	}
	elsif(/^\s+KeyRange=(\d+)-(\d+)/)
	{
		$ins{$last_seen}->{min} = $1 + $offset;
		$ins{$last_seen}->{max} = $2 + $offset;

		# set base as min if it's not looped, as they
		# seem to have always 60
		$ins{$last_seen}->{base} = $ins{$last_seen}->{min}
			unless $ins{$last_seen}->{start};
	}
	elsif(/^\s+Sample: "$last_seen",.*UnityNote=(\d+)/)
	{
		# set only if wave is looped
		if($ins{$last_seen}->{start})
		{
			$ins{$last_seen}->{base} = $1 + $offset;
		}
	}
}


print "/* converted by gigdump2ahs.pl $VERSION */\n\n";

foreach my $i (@ins)
{
	$i = $ins{$i};
	print "{ wav \"$i->{filename}\" $i->{base} $i->{min} $i->{max} $i->{start} $i->{end} }\n";
}
