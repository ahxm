/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2011 Angel Ortega <angel@triptico.com>

    main.c - Miscellaneous functions and startup

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "ahxm.h"


/** data **/

/* output information */

static char *devfile = NULL;
static char *driver = "default";

/* seconds to skip */
static int skip_secs = 0;


/** code **/

static void read_config(void)
{
}


static int main_usage(void)
{
    printf("Ann Hell Ex Machina %s - Music Writing Software\n", VERSION);
    printf("Copyright (C) 2003-2011 Angel Ortega <angel@triptico.com>\n");
    printf("This software is covered by the GPL license. NO WARRANTY.\n\n");
    printf("Usage: ahxm [options] {script.ahs}\n\n");
    printf("Options:\n\n");
    printf("-o {output file or device} Set output file [default: driver dependent].\n");
    printf("-d {driver}                Output driver [default: 'default'] (see below).\n");
    printf("-L {path}                  Add path to script / data library search path.\n");
    printf("-r {sampling rate}         Set sampling rate [default: %d]\n",
           ss_frequency);
    printf("-c {# of channels}         Force number of channels [default: use all]\n");
    printf("-v {master volume}         Set master volume [default: %02.02f]\n",
         ss_master_volume);
    printf("-s {seconds to skip}       Seconds to skip from the start of the song.\n");
    printf("                           [default: 0, start from the beginning]\n");
    printf("-z {track number}          Play track number in a solo\n");
    printf("-i {interpolation}         Set interpolation algorithm\n");
    printf("                           (0, none; 1, linear; 2, cubic spline,\n");
    printf("                           3, Lagrange) [default: %d]\n",
           ss_interpolation);
    printf("-b {size in frames}        Set maximum wave buffer size in frames, so\n");
    printf("                           wave files bigger than this will be paged\n");
    printf("                           (-1, load full wave in memory) [default: %d]\n",
         ss_page_size);
    printf("-D {number}                Verbosity level (0-3) [default: %d].\n",
         verbose);
    printf("-C {cue file name}         Generate .cue file.\n");
    printf("-T                         Show tracing information.\n");
    printf("-M                         Output song to MIDI device instead of softsynth.\n");
    printf("-I                         Interactive; read from MIDI device and\n");
    printf("                           use {script} as instrument (UNIMPLEMENTED).\n");
    printf("\n");
    printf("Internal sample size: %d bytes (floating point)\n",
           (int) sizeof(sample_t));
    printf("Library path: ");
    libpath_print();
    printf("\n");
    printf("Configured drivers: %s\n", CONFOPT_DRIVERS);

    return 1;
}


static int main_interactive(char *script_or_ins)
{
    printf("UNIMPLEMENTED; use the 'midiin' program instead.\n");
    return 0;
}


static int main_midi(char *script)
{
    if (compile_ahs(script))
        return 3;

    if (midi_device_open(devfile) < 0) {
        printf("Error: can't open MIDI device\n");
        return 2;
    }

    midi_song_play(skip_secs);

    midi_device_close();

    return 0;
}


static int main_ss(char *script)
{
    char *ptr;

    /* is it an .sf2? create an ad-hoc mini program to list it */
    if ((ptr = strrchr(script, '.')) != NULL && strcmp(ptr, ".sf2") == 0) {
        char tmp[4096];

        snprintf(tmp, sizeof(tmp), "{ sf2 \"%s\" }", script);
        if (compile_ahs_string(tmp))
            return 4;
    }
    else
    if (compile_ahs(script))
        return 3;

    ss_song_render(skip_secs, driver, devfile);

    if (verbose >= 1)
        printf("clipped: %d optimal volume: %f\n",
               ss_output_clipped, ss_optimal_volume);

    return 0;
}


static int ahxm_main(int argc, char *argv[])
{
    int ret = 1;
    int n = 1;
    char *opt = NULL;
    char *script = NULL;
    int midi = 0;
    int interactive = 0;

    read_config();

    /* default path */
    libpath_add("~/ahxmlib", 0);

    /* specific converters */
    transconv_add(".flac", ".wav", "flac -d -s -o '%s' '%s'");
    transconv_add(".mp3", ".wav", "mpg321 -q -w '%s' '%s'");

    /* fallout converter */
    transconv_add(".*", ".wav", "ffmpeg '%s' -i '%s'");

    if (argc == 1)
        return main_usage();

    while (n < argc - 1) {
        opt = argv[n++];

        if (strcmp(opt, "-o") == 0) {
            /* set output device or file */
            devfile = argv[n++];
        }
        else
        if (strcmp(opt, "-d") == 0) {
            /* set driver */
            driver = argv[n++];
        }
        else
        if (strcmp(opt, "-L") == 0) {
            /* add path to library */
            libpath_add(argv[n++], 0);
        }
        else
        if (strcmp(opt, "-r") == 0) {
            /* set sampling rate */
            ss_frequency = atoi(argv[n++]);
        }
        else
        if (strcmp(opt, "-c") == 0) {
            /* set number of channels */
            ss_nchannels = atoi(argv[n++]);
        }
        else
        if (strcmp(opt, "-v") == 0) {
            /* set master volume */
            ss_master_volume = (double) atof(argv[n++]);
        }
        else
        if (strcmp(opt, "-i") == 0) {
            /* set interpolation type */
            ss_interpolation = atoi(argv[n++]);
        }
        else
        if (strcmp(opt, "-s") == 0) {
            /* seconds to skip */
            skip_secs = atoi(argv[n++]);
        }
        else
        if (strcmp(opt, "-I") == 0) {
            /* activate interactive mode */
            interactive = 1;
        }
        else
        if (strcmp(opt, "-M") == 0) {
            /* activate MIDI mode */
            midi = 1;
        }
        else
        if (strcmp(opt, "-z") == 0) {
            /* set solo track */
            solo_track = atoi(argv[n++]);
        }
        else
        if (strcmp(opt, "-b") == 0) {
            /* set page size */
            ss_page_size = atoi(argv[n++]);

            /* if it's -1, set as 'no limit' */
            if (ss_page_size < 0)
                ss_page_size = INT_MAX;
        }
        else
        if (strcmp(opt, "-D") == 0) {
            /* debug information */
            verbose = atoi(argv[n++]);
        }
        else
        if (strcmp(opt, "-C") == 0) {
            /* set cue file name */
            ss_cue_file_name = argv[n++];
        }
        else
        if (strcmp(opt, "-T") == 0)
            trace = 1;
        else
            return main_usage();
    }

    script = argv[argc - 1];

    if (*script == '-')
        return main_usage();

    if (interactive)
        ret = main_interactive(script);
    else
    if (midi)
        ret = main_midi(script);
    else
        ret = main_ss(script);

    return ret;
}


int main(int argc, char *argv[])
{
    return ahxm_main(argc, argv);
}
