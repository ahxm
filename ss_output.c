/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    ss_output.c - Softsynth output interface

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <signal.h>

#include "ahxm.h"


/** data **/

/* master output volume */
sample_t ss_master_volume = 0.5;

/* output accounting */
int ss_output_clipped = 0;
sample_t ss_max = 0.0;

/* optimal output volume */
sample_t ss_optimal_volume;

/* driver data */
static void *outdev;

/*
	usual channel mapping:

	mono: all
	stereo: left | right
	3 channel: left | right | center
	quad: front left | front right | rear left | rear right
	4 channel: left | center | right | surround
	6 channel: left center | left | center | right center | right | surround
	home cinema 5.1: front l | center | front r | real l | rear r | subwoofer

	Converting from wav to 5.1 ac3:

	ffmpeg -ac 6 -ab 192 -ar 44100 -i input.wav output.ac3
*/

/* a copy of the output file name */
static const char *ss_output_file_name = NULL;

/* cue file name and track */
const char *ss_cue_file_name = NULL;
static int ss_cue_file_track = -1;


/** code **/

void *ss_outdev_open(const char *drvname, const char *file, int freq,
                     int n_channels);
void ss_outdev_write(void *drv, short int *frame);
void *ss_outdev_close(void *drv);


static void close_on_signal(int sig_num)
/* SIGINT (and other) signal handler */
{
    /* close output device */
    outdev = ss_outdev_close(outdev);

    /* exit */
    exit(0);
}


/**
 * ss_output_open - Opens an output device.
 * @name: name of the driver
 * @filename: name of the file or device
 *
 * Opens an output device. @name contains the name of the driver
 * (i.e. "oss" or "wav"), @filename contains the (optional) name
 * of the output file or device (i.e. a filename
 * "wav" or "/dev/dsp" for a direct audio device like "oss").
 * @Name can be the special pseudo-driver "default"
 * to select the most appropriate (usually a platform-specific
 * direct output device, or "wav" if no one exists).
 * @filename can also be NULL; in that case, a driver dependent,
 * default value is used.
 *
 * Returns zero if the output device was correctly open, or
 * nonzero otherwise.
 */
int ss_output_open(const char *drvname, const char *filename)
{
    if (strcmp(drvname, "default") == 0)
        drvname = NULL;

    /* reset accounting */
    ss_output_clipped = 0;
    ss_max = 0.0;

    /* close the device on unexpected signals */
    signal(SIGINT, close_on_signal);

    /* store a copy of the filename */
    ss_output_file_name = filename;

    return (outdev = ss_outdev_open(drvname,
                                    filename, ss_frequency,
                                    ss_nchannels)) == NULL;
}


/**
 * ss_output_init_frame - Inits a frame
 * @frame: the frame
 *
 * Inits a frame, setting all samples to zero.
 */
void ss_output_init_frame(sample_t frame[])
{
    memset(frame, '\0', sizeof(sample_t) * SS_MAX_CHANNELS);
}


/**
 * ss_output_write - Outputs a frame of samples.
 * @frame: the frame of samples
 *
 * Outputs a frame of samples. Applies master volume, tests for
 * maximum amplitudes and clips all saturating ones.
 *
 * Returns a negative value in case of error, or 0 otherwise.
 */
int ss_output_write(sample_t frame[])
{
    int n, ret = 0;
    sample_t s;
    short int is[SS_MAX_CHANNELS];

    /* final corrections */
    for (n = 0; n < ss_nchannels; n++) {
        s = frame[n];

        /* test maximum amplitudes, apply master
           volume and clipping */
        if (s < 0) {
            if (s < -ss_max)
                ss_max = -s;
            s *= ss_master_volume;
            if (s < -1) {
                s = -1;
                ss_output_clipped++;
            }
        }
        else {
            if (s > ss_max)
                ss_max = s;
            s *= ss_master_volume;
            if (s > 1) {
                s = 1;
                ss_output_clipped++;
            }
        }

        /* convert to 16 bit signed */
        s *= 32767.0;

        /* store in buffer */
        is[n] = (short int) s;
    }

    /* finally write */
    ss_outdev_write(outdev, is);

    return ret;
}


/**
 * ss_output_close - Closes the output device.
 *
 * Closes the output driver.
 */
void ss_output_close(void)
{
    /* back to default signal behaviour */
    signal(SIGINT, SIG_DFL);

    /* close the device */
    outdev = ss_outdev_close(outdev);

    /* calculate optimal master volume for zero saturation */
    if (ss_max)
        ss_optimal_volume = 1 / ss_max;
}


static int cue_file_init(void)
/* inits the cue file, writing the first line */
{
    FILE *f;

    if (ss_output_file_name == NULL
        || (f = fopen(ss_cue_file_name, "wb")) == NULL)
        return -2;

    /* write first line */
    fprintf(f, "FILE \"%s\" WAVE\n", ss_output_file_name);
    fclose(f);

    ss_cue_file_track = 0;

    return 0;
}


int cue_file_song_info(int frame, const char *author, const char *name)
{
    int s;
    FILE *f;

    if (ss_cue_file_name == NULL)
        return 0;

    /* init if it's the first time */
    if (ss_cue_file_track == -1) {
        /* can't open? fail */
        if (cue_file_init() < 0) {
            ss_cue_file_name = NULL;
            return -1;
        }
    }

    /* open or fail */
    if ((f = fopen(ss_cue_file_name, "a")) == NULL)
        return -1;

    s = frame / ss_frequency;

    fprintf(f, "TRACK %02d AUDIO\n", ss_cue_file_track++);
    fprintf(f, "  PERFORMER \"%s\"\n", author);
    fprintf(f, "  TITLE \"%s\"\n", name);
    fprintf(f, "  INDEX 01 %02d:%02d:00\n", s / 60, s % 60);

    fclose(f);
    return 0;
}
