/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    ss_gen.c - Software syntesizer's sound generators

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ahxm.h"


/** data **/

/* maximum number of generators */
int ss_gen_num = SS_MAX_GENERATORS;

/* generator pool */
static struct ss_gen *ss_gen_pool = NULL;

/* free generator queue */
static struct ss_gen *ss_gen_free_queue = NULL;


/** code **/

static void ss_gen_enqueue(struct ss_gen **q, struct ss_gen *g)
/* Enqueues a generator in a generator queue */
{
    g->prev = NULL;
    g->next = *q;

    if (*q != NULL)
        (*q)->prev = g;

    *q = g;
}


void ss_gen_init(void)
/* inits the generator pool */
{
    int n;

    ss_gen_pool = (struct ss_gen *) realloc(ss_gen_pool, ss_gen_num *
                                            sizeof(struct ss_gen));

    memset(ss_gen_pool, '\0', ss_gen_num * sizeof(struct ss_gen));

    /* enqueue all into the free generator queue */
    for (n = 0; n < ss_gen_num; n++)
        ss_gen_enqueue(&ss_gen_free_queue, &ss_gen_pool[n]);
}


static struct ss_gen *ss_gen_dequeue(struct ss_gen **q, struct ss_gen *g)
/* Dequeues a generator from a generator queue */
{
    if (g->prev != NULL)
        g->prev->next = g->next;
    else
        *q = g->next;

    if (g->next != NULL)
        g->next->prev = g->prev;

    return g;
}


static struct ss_gen *ss_gen_pop(struct ss_gen **q)
/* gets the first enqueued generator from q */
{
    struct ss_gen *g = NULL;

    if (*q != NULL)
        g = ss_gen_dequeue(q, *q);

    return g;
}


/**
 * ss_gen_alloc - Allocs and enqueues a generator
 * @q: queue where the generator will be enqueued
 *
 * Allocs and enqueues a generator into the @q queue.
 *
 * The new generator is returned, or NULL if the
 * generator pool is empty.
 */
struct ss_gen *ss_gen_alloc(struct ss_gen **q)
{
    struct ss_gen *g;

    if ((g = ss_gen_pop(&ss_gen_free_queue)) != NULL)
        ss_gen_enqueue(q, g);

    return g;
}


/**
 * ss_gen_free - Dequeues a generator and frees it
 * @q: the queue holding the generator
 * @g: the generator
 *
 * Dequeues a generator and sends it back to the generator pool.
 */
void ss_gen_free(struct ss_gen **q, struct ss_gen *g)
{
    ss_gen_enqueue(&ss_gen_free_queue, ss_gen_dequeue(q, g));
}


/**
 * ss_gen_sustain - Sets sustain for a generator
 * @g: the generator
 * @sustain: sustain time in msecs
 *
 * Sets sustain for a generator, where @sustain is expressed
 * in milliseconds.
 */
void ss_gen_sustain(struct ss_gen *g, double sustain)
{
    g->sustain = MS2F(sustain);
}


/**
 * ss_gen_attack - Sets attack for a generator
 * @g: the generator
 * @attack: attack time in msecs
 *
 * Sets attack for a generator, where @attack is expressed
 * in milliseconds.
 */
void ss_gen_attack(struct ss_gen *g, double attack)
{
    if ((g->attack = MS2F(attack)) > 0) {
        /* calculates the delta volume */
        g->davol = g->vol / (sample_t) g->attack;

        g->vol = 0.0;
    }
}


/**
 * ss_gen_vibrato - Sets vibrato for a generator
 * @g: the generator
 * @depth: vibrato depth in msecs
 * @freq: vibrato frequency
 *
 * Sets vibrato for a generator, with a @depth expressed in
 * milliseconds and a frequency @freq expressed in hzs.
 */
void ss_gen_vibrato(struct ss_gen *g, double depth, double freq)
{
    /* good freq: 6 Hz (0.001) */
    /* good depth: 1/6 semitone (20, 30 frames) */

    g->vib_depth = MS2F(depth);
    g->vib_inc = (6.28 * freq) / (double) ss_frequency;
    g->vibrato = 0.0;
}


/**
 * ss_gen_portamento - Sets portamento for a generator
 * @g: the generator
 * @portamento: portamento value
 *
 * Sets portamento for a generator, where @portamento is an
 * increment to the internal cursor of the wave. This value must
 * be very small. Negative values will make the frequency slide
 * down and positive slide up.
 */
void ss_gen_portamento(struct ss_gen *g, double portamento)
{
    g->portamento = portamento;
}


/**
 * ss_gen_play - Activates a generator.
 * @g: generator
 * @freq: frequency of the sound to be generated
 * @vol: volume
 * @note_id: note id
 * @w: the wave
 *
 * Activates a generator, usually as a response for a 'note on'
 * message from an upper level. The wave @w holds all the sample
 * data (PCM data, base frequency, etc.), @freq is the desired
 * frequency, @vol the volume and @note_id a positive, unique
 * identifier for the note.
 */
void ss_gen_play(struct ss_gen *g, double freq, sample_t vol, int note_id,
                 struct ss_wave *w)
{
    /* store data */
    g->note_id = note_id;
    g->vol = vol;
    g->w = w;

    /* start from the beginning */
    g->cursor = 0;

    /* calculate increment */
    g->inc = freq / w->base_freq;
    g->inc *= (double) w->s_rate / (double) ss_frequency;

    /* default sustain, vibrato and portamento */
    ss_gen_sustain(g, 50.0);
    ss_gen_attack(g, 0.0);
    ss_gen_vibrato(g, 0.0, 0.0);
    ss_gen_portamento(g, 0.0);
}


/**
 * ss_gen_release - Releases a generator.
 * @g: the generator
 *
 * Releases a generator, usually as a response for a 'note off'
 * message from an upper level. The generator enters SS_GEN_RELEASED
 * mode, which starts generating sustain data until it's over.
 */
void ss_gen_release(struct ss_gen *g)
{
    /* note needs not be tracked anymore */
    g->note_id = -1;

    /* calculates the delta volume */
    g->dsvol = g->vol / (sample_t) g->sustain;
}


/**
 * ss_gen_frame - Generates a frame of samples.
 * @g: the generator
 * @n_channels: the desired number of channels
 * @frame: array where the output samples will be stored
 *
 * Generates a frame of samples from the @g generator, that will be stored
 * in the @frame array. If @n_channels is greater than the number
 * of channels the generator has, they are sequentially spread.
 *
 * Returns non-zero if the generator is stopped and should be freed.
 */
int ss_gen_frame(struct ss_gen *g, int n_channels, sample_t frame[])
{
    int n, m;
    int to_free = 0;
    double v;
    sample_t l_frame[SS_MAX_CHANNELS];
    struct ss_wave *w;

    v = g->cursor;
    w = g->w;

    /* process attack */
    if (g->attack) {
        g->vol += g->davol;
        g->attack--;
    }

    /* process vibrato */
    if (g->vib_inc) {
        g->vibrato += g->vib_inc;
        v += sin(g->vibrato) * g->vib_depth;
    }

    /* add samples to frame */
    for (n = 0; n < w->n_channels; n++)
        l_frame[n] = ss_get_sample(w, n, v) * g->vol;

    /* spread the frame into n_channels */
    for (n = w->first_channel, m = 0; n < n_channels; n++) {
        frame[n] += l_frame[m];

        n += w->skip_channels;
        if (++m == w->n_channels)
            m = 0;
    }

    /* increment pointer */
    g->cursor += g->inc;

    /* test loop boundaries */
    if (g->cursor > w->loop_end) {
        /* loop mode? */
        if (w->loop_start < 0)
            to_free = 1;
        else
            g->cursor = w->loop_start;
    }

    /* process sustain */
    if (g->note_id == -1) {
        g->vol -= g->dsvol;

        if (--g->sustain <= 0)
            to_free = 1;
    }

    /* process portamento */
    g->inc += g->portamento;

    return to_free;
}
