/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    ss_core.c - Softsynth core functions

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ahxm.h"


/** data **/

/* main output frequency */
int ss_frequency = 44100;

/* interpolation type: 0, none; 1, linear; 2, cubic spline; 3, lagrange */
int ss_interpolation = 3;

/* output channels */
int ss_nchannels = -1;

/* note frequencies */
double ss_middle_A_freq = 440.0;
static double note_frequency[128];


/** code **/

/**
 * ss_note_frequency - MIDI note to frequency converter
 * @note: the MIDI note
 *
 * Accepts a MIDI note number (range 0 to 127) and
 * returns its frequency in Hz.
 */
double ss_note_frequency(int note)
{
    int n;

    if (note < 0 || note > 127)
        return 0;

    /* builds the table if empty */
    if (note_frequency[0] == 0.0) {
        for (n = 0; n < 128; n++)
            note_frequency[n] = (ss_middle_A_freq / 32.0) *
                pow(2.0, (((double) n - 9.0) / 12.0));
    }

    return note_frequency[note];
}


/**
 * ss_alloc_wave - Allocates a wave structure.
 * @size: size in frames
 * @n_channels: number of channels
 * @s_rate: sampling rate
 * @p_size: size of the sound page
 *
 * Allocates a wave structure. If @p_size is -1, it's assumed to be the
 * same as @size (so the sound will live entirely in memory).
 */
struct ss_wave *ss_alloc_wave(int size, int n_channels, int s_rate,
                              int p_size)
{
    struct ss_wave *w = NULL;

    if (p_size == -1)
        p_size = size;

    if ((w = (struct ss_wave *) malloc(sizeof(struct ss_wave))) != NULL) {
        memset(w, '\0', sizeof(struct ss_wave));

        w->size = (double) size;
        w->p_size = p_size;
        w->n_channels = n_channels;
        w->s_rate = s_rate;

        /* alloc space for the pointers to the waves */
        w->wave = (sample_t **) malloc(n_channels * sizeof(sample_t *));
        memset(w->wave, '\0', n_channels * sizeof(sample_t *));
    }

    return w;
}


/**
 * ss_free_wave - Frees a wave structure.
 * @w: the wave structure
 *
 * Frees a struct ss_wave allocated by ss_alloc_wave().
 */
void ss_free_wave(struct ss_wave *w)
{
    if (w->wave != NULL) {
        int n;

        /* frees the buffers */
        for (n = 0; n < w->n_channels; n++)
            if (w->wave[n] != NULL)
                free(w->wave[n]);

        /* frees the space for the pointers to the waves */
        free(w->wave);

        /* if it has a filename, also free it */
        if (w->filename != NULL)
            free((char *) w->filename);
    }

    /* frees the wave itself */
    free(w);
}


void ss_prepare_wave(struct ss_wave *w)
/* prepares a wave file for usage (creates the page buffers) */
{
    int n;

    /* alloc space for the waves themselves */
    for (n = 0; n < w->n_channels; n++) {
        w->wave[n] =
            (sample_t *) realloc(w->wave[n], w->p_size * sizeof(sample_t));

        memset(w->wave[n], '\0', w->p_size * sizeof(sample_t));
    }
}


static void ss_load_page(struct ss_wave *w, int offset)
/* loads a page from a wave file into memory */
{
    FILE *f;
    int s;

    /* set the offset to some samples behind the
       wanted offset (to avoid possible page bounces) */
    if ((w->p_offset = offset - 441) < 0)
        w->p_offset = 0;

    /* too much page faults for this wave? */
    if (w->page_faults >= 8) {
        /* increase space */
        if ((w->p_size *= 2) > (int) w->size) {
            /* if this resize is too much, just
               set it to load the full wave */
            w->p_size = (int) w->size;
            w->p_offset = 0;
        }

        /* trigger a page resizing and restart statistics */
        w->page_faults = 0;
    }

    if (w->page_faults == 0)
        ss_prepare_wave(w);

    if ((f = fopen(w->filename, "rb")) == NULL) {
        fprintf(stderr, "Can't open '%s'\n", w->filename);
        return;
    }

    if (verbose >= 3)
        printf("load_page [%s,%d,%d,%d]\n", w->filename,
               w->p_offset, w->p_size, w->page_faults);

    /* calculate the frame size */
    s = w->p_offset * (w->bits / 8) * w->n_channels;

    /* move there */
    fseek(f, w->f_pos + s, SEEK_SET);

    /* fill the page */
    load_pcm_wave(f, w);

    fclose(f);

    w->page_faults++;
}


static sample_t ss_pick_sample(struct ss_wave *w, int channel,
                               double offset)
/* picks a sample from a ss_wave, forcing a call to ss_load_page() if
   the wanted sample is not in memory */
{
    int o;
    sample_t *wave;

    o = (int) offset;

    /* is the wanted sample not in memory? */
    if (o < w->p_offset || o > w->p_offset + w->p_size) {

        if (verbose >= 3)
            printf
                ("ss_pick_sample: offset: %lf, p_offset: %d, p_size: %d, size: %lf, loop_end: %lf\n",
                 offset, w->p_offset, w->p_size, w->size, w->loop_end);

        ss_load_page(w, o);
    }

    wave = w->wave[channel];
    return wave[o - w->p_offset];
}


/**
 * ss_get_sample - Reads a sample from a wave.
 * @wave: the wave
 * @channel: the channel
 * @offset: sample number to be returned
 *
 * Returns the sample number @offset from the @channel of the @wave. @Offset
 * can be a non-integer value.
 */
sample_t ss_get_sample(struct ss_wave * w, int channel, double offset)
{
    sample_t d, t, r = 0.0;
    sample_t s0, s1, s2, s3;

    /* take care of wrappings */
    if (offset < 0)
        offset += w->size;

    /* pick sample at offset */
    s1 = ss_pick_sample(w, channel, offset);

    switch (ss_interpolation) {
    case 0:
        /* no interpolation */
        r = s1;
        break;

    case 1:
        /* linear interpolation */
        if (offset > w->size - 2)
            r = s1;
        else {
            d = (sample_t) (offset - floor(offset));
            s2 = (ss_pick_sample(w, channel, offset + 1) - s1) * d;

            r = s1 + s2;
        }

        break;

    case 2:
        /* cubic spline (borrowed from timidity) */
        if (offset < 1 || offset > w->size - 3)
            r = s1;
        else {
            s0 = ss_pick_sample(w, channel, offset - 1);
            s2 = ss_pick_sample(w, channel, offset + 1);
            s3 = ss_pick_sample(w, channel, offset + 2);

            t = s2;

            d = (sample_t) (offset - floor(offset));

            s2 = (6.0 * s2 +
                  ((5.0 * s3 - 11.0 * s2 + 7.0 * s1 - s0) / 4.0) *
                  (d + 1.0) * (d - 1.0)) * d;

            s1 = ((6.0 * s1 +
                   ((5.0 * s0 - 11.0 * s1 + 7.0 * t - s3) / 4.0) *
                   d * (d - 2.0)) * (1.0 - d) + s2) / 6.0;

            r = s1;
        }

        break;

    case 3:
        /* lagrange (borrowed from timidity) */
        if (offset < 1 || offset > w->size - 3)
            r = s1;
        else {
            s0 = ss_pick_sample(w, channel, offset - 1);
            s2 = ss_pick_sample(w, channel, offset + 1);
            s3 = ss_pick_sample(w, channel, offset + 2);

            d = (sample_t) (offset - floor(offset));

            s3 += -3.0 * s2 + 3.0 * s1 - s0;
            s3 *= (d - 2.0) / 6.0;
            s3 += s2 - 2.0 * s1 + s0;
            s3 *= (d - 1.0) / 2.0;
            s3 += s1 - s0;
            s3 *= d;
            s3 += s0;

            r = s3;
        }

        break;
    }

    return r;
}


/**
 * ss_tempo_from_wave - Calculates a tempo from a wave
 * @w: the wave
 * @note: note to calculate the tempo from
 * @len: whole notes the tempo should match
 *
 * Calculates the optimal tempo for the @w wave, playing the @note,
 * to last @len whole notes.
 */
double ss_tempo_from_wave(const struct ss_wave *w, int note, double len)
{
    double d;

    d = ss_note_frequency(note) / w->base_freq;

    /* get the length of a whole, in seconds */
    d *= w->s_rate / w->size;

    /* convert to minutes */
    d *= 60.0;

    /* then to bpm,s */
    d *= 4.0;
    d *= len;

    return d;
}


/**
 * ss_pitch_from_tempo - Calculates a pitch from a tempo
 * @w: the wave
 * @tempo: current tempo
 * @len: desired length in whole notes
 *
 * Calculates the optimal frequency (pitch) for the @w wave, at @tempo,
 * to last @len whole notes.
 */
double ss_pitch_from_tempo(const struct ss_wave *w, double tempo,
                           double len)
{
    double d;

    /* calculate number of seconds the wave lasts */
    d = w->size / (double) w->s_rate;

    /* convert to minutes, then to wpms */
    d /= 60.0;
    d *= (tempo / 4.0);

    return w->base_freq * d * len;
}
