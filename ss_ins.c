/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    ss_ins.c - Software synthesizer's instruments

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ahxm.h"


/**
 * ss_ins_init - Initializes an instrument.
 * @i: the instrument
 *
 * Initializes an instrument structure.
 */
void ss_ins_init(struct ss_ins *i)
{
    memset(i, '\0', sizeof(struct ss_ins));

    /* default channels: stereo left and right, full volume */
    ss_ins_set_channel(i, 0, 1.0);
    ss_ins_set_channel(i, 1, 1.0);

    /* sustain shouldn't be 0 to avoid end of note clicks */
    ss_ins_set_sustain(i, 50.0);
    ss_ins_set_attack(i, 0.0);
    ss_ins_set_vibrato(i, 0.0, 0.0);
    ss_ins_set_portamento(i, 0.0);
}


/**
 * ss_ins_disable - Disables an instrument.
 * @i: the instrument
 *
 * Disables an instrument. From now on, no more notes can be played
 * on this instrument. If no note are left sounding, ss_ins_frame()
 * will return immediately.
 */
void ss_ins_disable(struct ss_ins *i)
{
    i->disabled = 1;
}


/**
 * ss_ins_add_layer - Adds a layer to an instrument.
 * @i: the instrument
 * @w: wave describing the layer
 *
 * Adds a layer to an instrument.
 *
 * Returns 0 if the layer was added successfully.
 */
void ss_ins_add_layer(struct ss_ins *i, struct ss_wave *w)
{
    /* grow layers */
    GROW(i->layers, i->n_layers, struct ss_wave *);

    i->layers[i->n_layers] = w;

    i->n_layers++;
}


/**
 * ss_ins_find_layer - Finds a layer inside an instrument
 * @i: the instrument
 * @freq: the desired frequency
 * @off: pointer to element offset to start searching
 *
 * Finds a layer inside the @i instrument with a matching @freq, starting
 * from the layer number pointed by @off. If a matching layer is found, its
 * struct ss_wave is returned and @off is left pointing to the next layer
 * (allowing it to be used as an enumerator). If no layer is found, NULL
 * is returned.
 */
struct ss_wave *ss_ins_find_layer(const struct ss_ins *i, double freq,
                                  int *off)
{
    int n = 0;
    struct ss_wave *w = NULL;

    /* if off is NULL, point to the first layer */
    if (off == NULL)
        off = &n;

    /* find a matching layer, starting from *off */
    for (; *off < i->n_layers; (*off)++) {
        w = i->layers[*off];

        if (freq >= w->min_freq && freq <= w->max_freq)
            break;
    }

    /* passed the end; none found */
    if (*off == i->n_layers)
        w = NULL;
    else
        (*off)++;

    return w;
}


/**
 * ss_ins_set_channel - Sets the volume for an instrument's channel.
 * @i: the instrument
 * @channel: channel number
 * @vol: volume
 *
 * Sets the volume for an instrument's channel. If the channel does
 * not exist, it's created and space allocated for it in the volume and
 * effect dynamic arrays.
 */
void ss_ins_set_channel(struct ss_ins *i, int channel, sample_t vol)
{
    /* if channel is new, alloc space for it */
    if (channel <= i->n_channels) {
        int n;

        GROW(i->vols, channel, sample_t);
        GROW(i->effs, channel, struct ss_eff *);

        /* fill newly allocated space */
        for (n = i->n_channels; n <= channel; n++) {
            i->vols[n] = 1.0;
            i->effs[n] = NULL;
        }

        i->n_channels = channel + 1;
    }

    /* store volume */
    i->vols[channel] = vol;
}


/**
 * ss_ins_set_sustain - Sets the sustain for an instrument.
 * @i: the instrument
 * @sustain: the sustain time in milliseconds
 *
 * Sets the sustain for an instrument. @sustain is expressed in
 * milliseconds.
 */
void ss_ins_set_sustain(struct ss_ins *i, double sustain)
{
    i->sustain = sustain;
}


/**
 * ss_ins_set_attack - Sets the attack for an instrument.
 * @i: the instrument
 * @attack: the attack time in milliseconds
 *
 * Sets the attack for an instrument. @attack is expressed in
 * milliseconds.
 */
void ss_ins_set_attack(struct ss_ins *i, double attack)
{
    i->attack = attack;
}


/**
 * ss_ins_set_vibrato - Sets the vibrato for an instrument.
 * @i: the instrument
 * @depth: vibrato depth in msecs
 * @freq: vibrato frequency in Hzs
 *
 * Sets the vibrato for an instrument. @depth is expressed in
 * milliseconds and @freq in Hzs.
 */
void ss_ins_set_vibrato(struct ss_ins *i, double depth, double freq)
{
    i->vib_depth = depth;
    i->vib_freq = freq;
}


/**
 * ss_ins_set_portamento - Sets portamento for an instrument.
 * @i: the instrument
 * @portamento: portamento value
 *
 * Sets portamento for an instrument.
 */
void ss_ins_set_portamento(struct ss_ins *i, double portamento)
{
    i->portamento = portamento;
}


/**
 * ss_ins_play - Plays a note given the desired wave.
 * @i: the instrument
 * @freq: frequency
 * @vol: volume
 * @note_id: note id
 * @w: the wave
 *
 * Orders the instrument to start playing a note, given a specific wave.
 * The wave is usually one of the instrument's layers, but it doesn't
 * have to.
 *
 * Returns -1 if the instrument is disabled, -2 if no free generators
 * were found, or 0 if everything went ok.
 */
int ss_ins_play(struct ss_ins *i, double freq, sample_t vol, int note_id,
                struct ss_wave *w)
{
    struct ss_gen *g;

    /* if the instrument is disabled, no more notes are allowed */
    if (i->disabled)
        return -1;

    /* get a free generator, or fail */
    if ((g = ss_gen_alloc(&i->gens)) == NULL)
        return -2;

    /* start the generator */
    ss_gen_play(g, freq, vol, note_id, w);

    /* set generator parameters */
    ss_gen_sustain(g, i->sustain);
    ss_gen_attack(g, i->attack);
    ss_gen_vibrato(g, i->vib_depth, i->vib_freq);
    ss_gen_portamento(g, i->portamento);

    return 1;
}


/**
 * ss_ins_note_on - Plays a note.
 * @i: the instrument
 * @note: MIDI note to be played
 * @vol: note volume
 * @note_id: note id
 *
 * Locates a layer to play a note, and starts generators to
 * play it. The @note is expressed as a MIDI note and the
 * desired volume (from 0 to 1) stored in @vol. The note @id
 * should be a positive, unique identifier for this note; no two
 * simultaneously playing notes should share this id.
 *
 * Returns the number of generators that were activated.
 */
int ss_ins_note_on(struct ss_ins *i, int note, sample_t vol, int note_id)
{
    int n, g;
    struct ss_wave *w;
    double freq;

    freq = ss_note_frequency(note);

    for (n = g = 0; (w = ss_ins_find_layer(i, freq, &n)) != NULL; g++) {
        if (ss_ins_play(i, freq, vol, note_id, w) < 0)
            break;
    }

    return g;
}


/**
 * ss_ins_note_off - Releases a note.
 * @i: the instrument
 * @note_id: the id of the note to be released
 *
 * Releases a note. The generators associated to it will enter release mode.
 */
void ss_ins_note_off(struct ss_ins *i, int note_id)
{
    struct ss_gen *g;

    /* releases all generators with that note_id */
    for (g = i->gens; g != NULL; g = g->next) {
        if (g->note_id == note_id)
            ss_gen_release(g);
    }
}


/**
 * ss_ins_frame - Generates a frame of samples.
 * @i: the instrument
 * @frame: array where the output samples will be stored
 *
 * Generates a frame of samples mixing all the active generators
 * of a track.
 *
 * Returns 0 if the instrument is disabled and is no longer
 * emitting sound, or 1 otherwise.
 */
int ss_ins_frame(struct ss_ins *i, sample_t frame[])
{
    struct ss_gen *g;
    struct ss_gen *t;
    int n;
    sample_t l_frame[SS_MAX_CHANNELS];

    /* if instrument is disabled and there is no more generators, exit */
    if (i->disabled && i->gens == NULL) {
        /* reset all effects */
        for (n = 0; n < i->n_channels; n++)
            ss_eff_off(&i->effs[n]);

        return 0;
    }

    /* resets this local frame */
    ss_output_init_frame(l_frame);

    /* loops through the generators */
    for (g = i->gens; g != NULL; g = t) {
        t = g->next;

        /* if the generator has stopped, free it */
        if (ss_gen_frame(g, i->n_channels, l_frame))
            ss_gen_free(&i->gens, g);
    }

    /* loops through the effects and remixes */
    for (n = 0; n < i->n_channels; n++)
        frame[n] += ss_eff_process(i->effs[n], l_frame[n] * i->vols[n]);

    return 1;
}
