#!/bin/sh

# Configuration shell script

BIN="ahxm midiin"

# gets program version
VERSION=`cut -f2 -d\" VERSION`

# default installation prefix
PREFIX=/usr/local

# parse arguments
while [ $# -gt 0 ] ; do

	case $1 in
	--help)			CONFIG_HELP=1 ;;

	--with-pthreads)	WITH_PTHREADS=1 ;;

	--prefix)		PREFIX=$2 ; shift ;;
	--prefix=*)		PREFIX=`echo $1 | sed -e 's/--prefix=//'` ;;
	esac

	shift
done

if [ "$CONFIG_HELP" = "1" ] ; then

	echo "Available options:"
	echo "--prefix=PREFIX       Installation prefix ($PREFIX)."
	echo "--with-pthreads       Activate POSIX threads support."

	echo
	echo "Environment variables:"
	echo "CC                    C Compiler."
	echo "CFLAGS                Compile flags (i.e., -O3)."
	echo "AR                    Archiver."
	echo "LEX                   Lexer."
	echo "YACC                  Parser."

	exit 1
fi

echo "Configuring..."

echo "/* automatically created by config.sh - do not modify */" > config.h
echo "# automatically created by config.sh - do not modify" > makefile.opts
> config.ldflags
> config.cflags
> .config.log

# set compiler
if [ "$CC" = "" ] ; then
	CC=cc
	# if CC is unset, try if gcc is available
	which gcc > /dev/null 2>&1

	if [ $? = 0 ] ; then
		CC=gcc
	fi
fi

echo "CC=$CC" >> makefile.opts

# set archiver
if [ "$AR" = "" ] ; then
	AR=ar
fi

echo "AR=$AR" >> makefile.opts

# set lexer
if [ "$LEX" = "" ] ; then
	LEX=lex
fi

echo "LEX=$LEX" >> makefile.opts

# set parser
if [ "$YACC" = "" ] ; then
	YACC=yacc
fi

echo "YACC=$YACC" >> makefile.opts

# add version
cat VERSION >> config.h

# add installation prefix
echo "#define CONFOPT_PREFIX \"$PREFIX\"" >> config.h

DRIVERS="wav"

#########################################################

# configuration directives

# CFLAGS
if [ -z "$CFLAGS" ] ; then
    CFLAGS="-g -Wall"
fi

echo -n "Testing if C compiler supports ${CFLAGS}... "
echo "int main(int argc, char *argv[]) { return 0; }" > .tmp.c

$CC .tmp.c -o .tmp.o 2>> .config.log

if [ $? = 0 ] ; then
    echo "OK"
else
    echo "No; resetting to defaults"
    CFLAGS=""
fi

echo "CFLAGS=$CFLAGS" >> makefile.opts

# Add CFLAGS to CC
CC="$CC $CFLAGS"

# Win32
echo -n "Testing for win32... "
if [ "$WITHOUT_WIN32" = "1" ] ; then
	echo "Disabled by user"
else
	echo "#include <windows.h>" > .tmp.c
	echo "#include <commctrl.h>" >> .tmp.c
	echo "int STDCALL WinMain(HINSTANCE h, HINSTANCE p, LPSTR c, int m)" >> .tmp.c
	echo "{ return 0; }" >> .tmp.c

	TMP_LDFLAGS="-lwinmm"
	$CC .tmp.c $TMP_LDFLAGS -o .tmp.o 2>> .config.log

	if [ $? = 0 ] ; then
		echo "#define CONFOPT_WIN32 1" >> config.h
		echo "OK"
		WITHOUT_UNIX_GLOB=1
        WITH_WIN32=1
        echo $TMP_LDFLAGS >> config.ldflags
        DRIVERS="win32 ${DRIVERS}"
	else
		echo "No"
	fi
fi

# test for Linux OSS
echo -n "Testing for Linux OSS... "
echo "#include <linux/soundcard.h>" > .tmp.c
echo "int main(void) {" >> .tmp.c
echo "int i=open(\"/dev/dsp\",0);" >> .tmp.c
echo "ioctl(i,SNDCTL_DSP_SETFRAGMENT,&i);" >> .tmp.c
echo "return 0; } " >>.tmp.c

$CC .tmp.c -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "#define CONFOPT_LINUX_OSS 1" >> config.h
	echo "OK"
    DRIVERS="oss ${DRIVERS}"
else
	echo "No"
fi

# test for IRIX audio library
echo -n "Testing for IRIX audio library... "
echo "#include <dmedia/audio.h>" > .tmp.c
echo "int main(void) { alNewConfig(); return 0; }" >> .tmp.c

$CC .tmp.c -laudio -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "#define CONFOPT_SGI 1" >> config.h
	echo "-laudio" >> config.ldflags
	echo "OK"
    DRIVERS="sgi ${DRIVERS}"
else
	echo "No"
fi

# test for esound library
echo -n "Testing for esound development libraries... "
echo "#include <esd.h>" > .tmp.c
echo "int main(void) { return 0; }" >> .tmp.c

$CC -I/usr/local/include -L/usr/local/lib -lesd .tmp.c -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "#define CONFOPT_ESD 1" >> config.h
	echo "-lesd" >> config.ldflags
	echo "OK"
    DRIVERS="esd ${DRIVERS}"
else
	echo "No"
fi

# test for artsc library
echo -n "Testing for aRts development libraries... "
echo "#include <artsc.h>" > .tmp.c
echo "int main(void) { arts_init(); return 0; }" >> .tmp.c

TMP_CFLAGS=`artsc-config --cflags 2>/dev/null`
TMP_LDFLAGS=`artsc-config --libs 2>/dev/null`

$CC $TMP_CFLAGS .tmp.c $TMP_LDFLAGS -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "#define CONFOPT_ARTS 1" >> config.h
	echo $TMP_CFLAGS >> config.cflags
	echo $TMP_LDFLAGS >> config.ldflags
	echo "OK"
    DRIVERS="arts ${DRIVERS}"
else
	echo "No"
fi

# test for pulseaudio libraries
echo -n "Testing for Pulseaudio development libraries... "
echo "#include <pulse/simple.h>" > .tmp.c
echo "#include <pulse/error.h>" >> .tmp.c
echo "int main(void) { return 0; }" >> .tmp.c

TMP_CFLAGS=$(pkg-config libpulse-simple --cflags 2>/dev/null)
TMP_LDFLAGS=$(pkg-config libpulse-simple --libs 2>/dev/null)

$CC $TMP_CFLAGS .tmp.c $TMP_LDFLAGS -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "#define CONFOPT_PULSEAUDIO 1" >> config.h
	echo $TMP_CFLAGS >> config.cflags
	echo $TMP_LDFLAGS >> config.ldflags
	echo "OK"
    DRIVERS="pulse ${DRIVERS}"
else
	echo "No"
fi

# test for getuid() availability
echo -n "Testing for getuid() existence... "
echo "#include <unistd.h>" > .tmp.c
echo "#include <sys/types.h>" >> .tmp.c
echo "int main(void) { getuid(); return 0; }" >> .tmp.c

TMP_CFLAGS=""
TMP_LDFLAGS=""
$CC $TMP_CFLAGS .tmp.c $TMP_LDFLAGS -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "OK"
else
	echo "No; activating workaround"
	echo "#define getuid() 1000" >> config.h
fi

# test for number of arguments in mkdir()
echo -n "Testing for number of arguments in mkdir()... "
echo "#include <stdio.h>" > .tmp.c
echo "#include <string.h>" >> .tmp.c
echo "#include <stdlib.h>" >> .tmp.c
echo "#include <unistd.h>" >> .tmp.c
echo "#include <sys/stat.h>" >> .tmp.c
echo "#include <sys/types.h>" >> .tmp.c
echo "int main(void) { mkdir(\"testdir\"); return 0; }" >> .tmp.c

$CC $TMP_CFLAGS .tmp.c $TMP_LDFLAGS -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "1"
	echo "#define CONFOPT_MKDIR_ARGS 1" >> config.h
else
	echo "2"
	echo "#define CONFOPT_MKDIR_ARGS 2" >> config.h
fi

echo -n "Testing for nanosleep() support... "
echo "#include <time.h>" > .tmp.c
echo "int main(void) { struct timespec ts; nanosleep(&ts, NULL); return 0; }" >> .tmp.c

TMP_CFLAGS=""
TMP_LDFLAGS=""
$CC $TMP_CFLAGS .tmp.c $TMP_LDFLAGS -o .tmp.o 2> /dev/null
if [ $? = 0 ] ; then
	echo "OK"
	echo "#define CONFOPT_NANOSLEEP 1" >> config.h
else
	echo "No; MIDI output will not be available"
	BIN="ahxm"
fi

# pthreads support
if [ "$WITH_PTHREADS" = 1 ] ; then
	echo "#define CONFOPT_PTHREADS 1" >> config.h
	echo "-pthread" >> config.ldflags
fi

# test for Grutatxt
echo -n "Testing if Grutatxt is installed... "

DOCS="\$(ADD_DOCS)"

if which grutatxt > /dev/null 2>&1 ; then
	echo "OK"
	echo "GRUTATXT=yes" >> makefile.opts
	DOCS="\$(GRUTATXT_DOCS)"
else
	echo "No"
	echo "GRUTATXT=no" >> makefile.opts
fi

# test for mp_doccer
echo -n "Testing if mp_doccer is installed... "
MP_DOCCER=$(which mp_doccer||which mp-doccer)

if [ $? = 0 ] ; then

	if ${MP_DOCCER} --help | grep grutatxt > /dev/null ; then

		echo "OK"

		echo "MP_DOCCER=yes" >> makefile.opts
		DOCS="$DOCS \$(MP_DOCCER_DOCS)"

		grep GRUTATXT=yes makefile.opts > /dev/null && DOCS="$DOCS \$(G_AND_MP_DOCS)"
	else
		echo "Outdated (No)"
		echo "MP_DOCCER=no" >> makefile.opts
	fi
else
	echo "No"
	echo "MP_DOCCER=no" >> makefile.opts
fi

#########################################################

# final setup

echo "BIN=$BIN" >> makefile.opts
echo "DOCS=$DOCS" >> makefile.opts
echo "VERSION=$VERSION" >> makefile.opts
echo "PREFIX=$PREFIX" >> makefile.opts
echo >> makefile.opts

cat makefile.opts makefile.in makefile.depend > Makefile

echo "#define CONFOPT_DRIVERS \"${DRIVERS}\"" >> config.h
echo
echo "Configured drivers: ${DRIVERS}"

#########################################################

# cleanup

rm -f .tmp.c .tmp.o

exit 0
