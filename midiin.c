/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    midiin.c - Code for testing the libraries

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>

#include "ahxm.h"


/** data **/

#define MIDI_NOTE_OFF   0x80
#define MIDI_NOTE_ON    0x90
#define MIDI_AFTERTOUCH 0xA0
#define MIDI_PARAMETER  0xB0
#define MIDI_PROGRAM    0xC0
#define MIDI_PRESSURE   0xD0
#define MIDI_PITCHWHEEL 0xE0
#define MIDI_SYSEX      0xF0
#define MIDI_EOX        0xf7

/* MIDI device */
char midi_dev[150] = "/dev/midi";
int midi_fd = -1;

/* instrument */
char instrument[150] = "samples/acpiano.pat";

/* the MIDI message */
unsigned char midimsg[128];
int i_msg = 0;

/* use the instruments in ss_ins */
extern struct ss_ins ss_song_ins[256];


/** code **/

void print_note(int note)
{
    static int octave = -1;
    int o, d, n;
    char *notestr[] = {
        "c", "c#", "d", "d#", "e", "f",
        "f#", "g", "g#", "a", "a#", "b"
    };

    o = note / 12;
    n = note % 12;

    if (octave == -1 || (d = (octave - o)) > 1 || d < -1) {
        /* first time or octave too far; set it */
        octave = o;
        printf("o%d ", o);
        d = 0;
    }

    printf("%s%s ", notestr[n], (d == -1 ? "'" : (d == 1 ? "," : "")));

    fflush(stdout);
}


void process_MIDI(void)
{
    /* strip MIDI channel */
    midimsg[0] &= 0xf0;

    if (midimsg[0] == MIDI_NOTE_OFF)
        ss_ins_note_off(&ss_song_ins[0], midimsg[1]);
    else
    if (midimsg[0] == MIDI_NOTE_ON) {
        ss_ins_note_on(&ss_song_ins[0], midimsg[1],
                       ((sample_t) midimsg[2]) / 127.0, midimsg[1]);

        print_note(midimsg[1]);
    }

    i_msg = 0;
}


void midiin(void)
{
    unsigned char midibuf[128];
    int c, n;
    sample_t sample[2];

    for (;;) {
        /* read from MIDI device */
        c = read(midi_fd, midibuf, sizeof(midibuf));

        if (c > 0) {
            for (n = 0; n < c; n++) {
                /* if 7bit of input is set,
                   it's a start of MIDI message */
                if ((midibuf[n] & 0x80) && i_msg)
                    process_MIDI();

                midimsg[i_msg++] = midibuf[n];
            }
        }

        ss_output_init_frame(sample);
        ss_ins_frame(&ss_song_ins[0], sample);
        ss_output_write(sample);
    }
}


int main(int argc, char *argv[])
{
    char *ext;

    ss_interpolation = 1;
    ss_frequency = 44100;
    ss_master_volume = 1;
    ss_nchannels = 2;

    libpath_add("~/ahxmlib", 0);

    /* default converters */
    transconv_add(".flac", ".wav", "flac -d -s -o '%s' '%s'");
    transconv_add(".mp3", ".wav", "mpg321 -q -w '%s' '%s'");

    if (argc == 0) {
        printf("Usage:\n");
        printf("midiin [instrument.pat] [midi device]\n");
        return 1;
    }

    if (argc > 1)
        strcpy(instrument, argv[1]);

    if (argc > 2)
        strcpy(midi_dev, argv[2]);

    ss_gen_init();

    /* loads the instrument or bangs */
    ss_ins_init(&ss_song_ins[0]);

    if ((ext = strchr(instrument, '.')) == NULL) {
        printf("The instrument '%s' doesn't have an extension\n",
               instrument);
        return 2;
    }

    if (strcmp(ext, ".pat") == 0 || strcmp(ext, ".PAT") == 0) {
        if (ss_load_pat_file(&ss_song_ins[0], instrument) < 0) {
            printf("Error loading '%s'\n", instrument);
            return 2;
        }
    }
    else {
        if (compile_ahs(instrument))
            return 2;

        if (ss_song_render(0, "default", NULL))
            return 4;
    }

    /* opens the MIDI channel */
    if ((midi_fd = open(midi_dev, O_RDONLY | O_NONBLOCK)) == -1) {
        printf("Error opening midi device '%s'\n", midi_dev);
        return 3;
    }

    midiin();

    ss_output_close();

    return 0;
}
