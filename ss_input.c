/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    ss_input.c - Code to load softsynth sounds in different formats

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ahxm.h"


/** data **/

/* maximum page size */
int ss_page_size = 441000;


/** code **/

static int fget16(FILE * f)
/* Reads a 16 bit integer from a file in big endian byte ordering */
{
    int c;

    c = fgetc(f);
    c += (fgetc(f) * 256);

    return c;
}


static int fget32(FILE * f)
/* Reads a 32 bit integer from a file in big endian byte ordering */
{
    int c;

    c = fgetc(f);
    c += (fgetc(f) * 256);
    c += (fgetc(f) * 65536);
    c += (fgetc(f) * 16777216);

    return c;
}


static sample_t load_sample(FILE * f, int bits, int sign)
/* loads one sample from a file */
{
    int s;

    /* if on eof, return silence */
    if (feof(f))
        return 0;

    if (bits == 8) {
        s = fgetc(f) - 128;
        s <<= 8;
    }
    else {
        if (!sign)
            s = fget16(f) - 32768;
        else
            s = (short int) fget16(f);
    }

    return ((sample_t) s) / 32768.0;
}


void load_pcm_wave(FILE * f, struct ss_wave *w)
/* loads an interleaved stream from a file */
{
    int n, m;

    /* fills the channels */
    for (m = 0; m < w->p_size; m++) {
        for (n = 0; n < w->n_channels; n++)
            w->wave[n][m] = load_sample(f, w->bits, w->sign);
    }
}


/**
 * ss_load_wav_file - Loads a file in .WAV format.
 * @file: name of the file
 * @base_freq: base frequency
 * @min_freq: minimum frequency
 * @max_freq: maximum frequency
 * @loop_start: frame number of loop start (-1, no loop)
 * @loop_end: frame number of loop end (-1, end of wave)
 * @first_channel: first channel to start spreading
 * @skip_channels: channels to skip when spreading
 *
 * Loads a file in .WAV format.
 */
struct ss_wave *ss_load_wav_file(const char *file,
                                 double base_freq, double min_freq,
                                 double max_freq, double loop_start,
                                 double loop_end, int first_channel,
                                 int skip_channels)
{
    FILE *f;
    char dummydata[256];
    int rlen, flen;
    short int b_per_sec, n_channels;
    char riffid[5], waveid[5], fmtid[5], dataid[5];
    double size;
    int s_rate, bits;
    struct ss_wave *w;
    int p;

    if (*file == '|')
        file = transconv_pipe(file + 1, ".wav", "ahxm");
    else {
        /* find the file in the library path */
        if ((file = libpath_locate(file)) == NULL)
            return NULL;

        /* try converting */
        if ((file = transconv(file, ".wav", "ahxm")) == NULL)
            return NULL;
    }

    if ((f = fopen(file, "rb")) == NULL)
        return NULL;

    fread(riffid, 1, 4, f);
    riffid[4] = 0;
    fread(&rlen, 1, 4, f);
    fread(waveid, 1, 4, f);
    waveid[4] = 0;

    if (strcmp(waveid, "WAVE")) {
        fclose(f);
        return NULL;
    }

    fread(fmtid, 1, 4, f);
    fmtid[4] = 0;
    flen = fget32(f);

    if (flen > 240)
        flen = 240;

    if (fget16(f) != 1) {
        /* wicked compressed format? fail */
        fclose(f);
        return NULL;
    }

    n_channels = fget16(f);
    s_rate = fget32(f);
    b_per_sec = fget32(f);

    bits = fget16(f) / n_channels;
    bits *= 8;

    fread(dummydata, 1, (size_t) flen - 14, f);
    fread(dataid, 1, 4, f);
    dataid[4] = 0;

    size = (double) fget32(f);
    if (bits == 16)
        size /= 2;
    size /= (double) n_channels;

    p = size > ss_page_size ? ss_page_size : size;

    if ((w = ss_alloc_wave(size, n_channels, s_rate, p)) != NULL) {
        w->base_freq = base_freq;
        w->min_freq = min_freq;
        w->max_freq = max_freq;

        w->loop_start = loop_start;

        if (loop_end < 0)
            w->loop_end = size;
        else
            w->loop_end = loop_end;

        w->first_channel = first_channel;
        w->skip_channels = skip_channels;

        /* fill the info needed for paging */
        w->filename = strdup(file);
        w->f_pos = ftell(f);
        w->bits = bits;
        w->sign = 1;

        /* set the page offset further the end, to
           force a page reading the first time it's used */
        w->p_offset = (int) size;
    }

    fclose(f);

    return w;
}


/**
 * ss_load_pat_file - Loads an instrument in .PAT format.
 * @i: The instrument
 * @filename: filename holding the instrument
 *
 * Loads data from a Gravis Ultrasound patch (.PAT) format and
 * stores it as layers for an instrument.
 *
 * Returns -100 if the file could not be read, -101 or -102
 * if the file is not recognized as a .PAT file, or 0 if
 * everything went OK.
 */
int ss_load_pat_file(struct ss_ins *i, const char *file)
{
    FILE *f;
    char buffer[512];
    int m, n, o;
    int n_layers;
    int flags, bits, sign, loop, pingpong;
    struct ss_wave *w;

    if ((f = libpath_fopen(file, "rb")) == NULL)
        return -100;

    /* read signatures */
    fread(buffer, 12, 1, f);
    if (strcmp(buffer, "GF1PATCH110") != 0) {
        fclose(f);
        return -101;
    }

    fread(buffer, 10, 1, f);
    if (strcmp(buffer, "ID#000002") != 0) {
        fclose(f);
        return -102;
    }

    /* skip description */
    fread(buffer, 65, 1, f);

    /* ignore volume */
    fget16(f);

    /* skip */
    fread(buffer, 109, 1, f);

    /* # of layers */
    n_layers = fgetc(f);

    /* skip */
    fread(buffer, 40, 1, f);

    for (n = 0; n < n_layers; n++) {
        int size, s_rate;
        double loop_start, loop_end;
        double min_freq, max_freq, base_freq;

        /* layer name */
        fread(buffer, 8, 1, f);

        size = (double) fget32(f);
        loop_start = (double) fget32(f);
        loop_end = (double) fget32(f);
        s_rate = fget16(f);

        min_freq = ((double) fget32(f)) / 1000.0;
        max_freq = ((double) fget32(f)) / 1000.0;
        base_freq = ((double) fget32(f)) / 1000.0;

        if (base_freq < 0)
            break;

        /* ignore fine-tune */
        fget16(f);

        /* ignore pan position */
        fgetc(f);

        /* skip envelope rate, value, tremolo and vibrato */
        fread(buffer, 18, 1, f);

        flags = fgetc(f);

        bits = flags & 0x01 ? 16 : 8;
        sign = flags & 0x02 ? 0 : 1;
        loop = flags & 0x04 ? 1 : 0;
        pingpong = flags & 0x08 ? 1 : 0;

        if (bits == 16) {
            size /= 2;
            loop_start /= 2;
            loop_end /= 2;
        }

        /* skip frequency scale data */
        fget16(f);
        fget16(f);

        /* skip reserved */
        fread(buffer, 36, 1, f);

        if ((w = ss_alloc_wave(size, 1, s_rate, -1)) == NULL)
            break;

        /* set the rest of values */
        w->loop_start = loop_start;
        w->loop_end = loop_end;
        w->base_freq = base_freq;
        w->min_freq = min_freq;
        w->max_freq = max_freq;
        w->bits = bits;
        w->sign = sign;

        /* load the wave */
        ss_prepare_wave(w);
        load_pcm_wave(f, w);

        if (pingpong && loop) {
            int loop_size;
            sample_t *ptr;

            /* if ping-pong, realloc space for a reverse
               version of the loop */
            loop_size = (int) (loop_end - loop_start);

            ptr = (sample_t *) malloc((size + loop_size + 1)
                                      * sizeof(sample_t));

            /* transfer start and loop */
            for (m = 0; m <= (int) loop_end; m++)
                ptr[m] = w->wave[0][m];

            /* transfer a reversed version of the loop */
            for (o = m - 1; o >= loop_start; o--, m++)
                ptr[m] = w->wave[0][o];

            /* transfer the end */
            for (o = loop_end + 1; o < size; o++, m++)
                ptr[m] = w->wave[0][o];

            w->loop_end += (double) loop_size;
            w->size += (double) loop_size;
            w->p_size += loop_size;

            /* free and swap */
            free(w->wave[0]);
            w->wave[0] = ptr;
        }

        if (loop == 0)
            w->loop_start = -1;

        /* finally add layer to instrument */
        ss_ins_add_layer(i, w);
    }

    fclose(f);
    return 0;
}


/* Soundfont support */

#define CID(a,b,c,d)    (((d)<<24)+((c)<<16)+((b)<<8)+((a)))

#define CID_RIFF  CID('R','I','F','F')
#define CID_LIST  CID('L','I','S','T')
#define CID_INFO  CID('I','N','F','O')
#define CID_sdta  CID('s','d','t','a')
#define CID_snam  CID('s','n','a','m')
#define CID_smpl  CID('s','m','p','l')
#define CID_pdta  CID('p','d','t','a')
#define CID_phdr  CID('p','h','d','r')
#define CID_pbag  CID('p','b','a','g')
#define CID_pmod  CID('p','m','o','d')
#define CID_pgen  CID('p','g','e','n')
#define CID_inst  CID('i','n','s','t')
#define CID_ibag  CID('i','b','a','g')
#define CID_imod  CID('i','m','o','d')
#define CID_igen  CID('i','g','e','n')
#define CID_shdr  CID('s','h','d','r')
#define CID_ifil  CID('i','f','i','l')
#define CID_isng  CID('i','s','n','g')
#define CID_irom  CID('i','r','o','m')
#define CID_iver  CID('i','v','e','r')
#define CID_INAM  CID('I','N','A','M')
#define CID_IPRD  CID('I','P','R','D')
#define CID_ICOP  CID('I','C','O','P')
#define CID_sfbk  CID('s','f','b','k')
#define CID_ICRD  CID('I','C','R','D')
#define CID_IENG  CID('I','E','N','G')
#define CID_ICMT  CID('I','C','M','T')
#define CID_ISFT  CID('I','S','F','T')

#define MAX_LAYERS	128

static int base_notes[MAX_LAYERS];
static int min_notes[MAX_LAYERS];
static int max_notes[MAX_LAYERS];
static int sampleids[MAX_LAYERS];

#define CALC_END(s,f) (ftell(f) + (s) + ((s) & 1))


int ss_load_sf2_file(struct ss_ins *i, const char *file, const char *iname)
{
    FILE *f;
    int n, num;
    int cid, fsize, fend;
    int sample_offset = 0;
    int ibag_s = -1, ibag_e = -1;
    int igen_s = -1, igen_e = -1;
    char tmp[21];
    int i_layers = 0;

    if ((f = libpath_fopen(file, "rb")) == NULL)
        return -100;

    if ((cid = fget32(f)) != CID_RIFF)
        return -101;

    fsize = fget32(f);
    fend = CALC_END(fsize, f);

    if ((cid = fget32(f)) != CID_sfbk)
        return -102;

    while (ftell(f) < fend) {
        int csize, cend;

        cid = fget32(f);
        csize = fget32(f);
        cend = CALC_END(csize, f);

        switch (cid) {
        case CID_LIST:

            cid = fget32(f);

            while (ftell(f) < cend) {
                int scid, ssize, send;

                scid = fget32(f);
                ssize = fget32(f);
                send = CALC_END(ssize, f);

                switch (cid) {
                case CID_INFO:

                    switch (scid) {
                    case CID_ifil:

                        if (fget16(f) < 2)
                            return -103;
                        fget16(f);
                        break;

                    case CID_INAM:
                    case CID_irom:
                    case CID_ICRD:
                    case CID_IENG:
                    case CID_IPRD:
                    case CID_ICOP:
                    case CID_ISFT:
                        /* misc. data (ignored) */
                        break;
                    }

                    fseek(f, send, SEEK_SET);
                    break;

                case CID_pdta:

                    switch (scid) {
                    case CID_phdr:
                        /* preset headers */
                        break;

                    case CID_pbag:
                        /* preset index list */
                        break;

                    case CID_pgen:
                        /* preset generator list */
                        break;

                    case CID_inst:
                        /* instrument names */
                        num = ssize / 22;

                        for (n = 0; n < num; n++) {
                            int i;

                            fread(tmp, 20, 1, f);
                            tmp[20] = '\0';

                            /* trim spaces */
                            for (i = 20; i && tmp[i] == '\0'; i--);
                            for (; i && tmp[i] == ' '; i--)
                                tmp[i] = '\0';

                            i = fget16(f);

                            /* if ibag_s is set and ibag_e isn't,
                               pick this as the end */
                            if (ibag_s != -1 && ibag_e == -1)
                                ibag_e = i;

                            if (iname != NULL) {
                                if (strcmp(iname, tmp) == 0)
                                    ibag_s = i;
                            }
                            else {
                                printf("[%s]\n", tmp);
                            }
                        }

                        break;

                    case CID_ibag:

                        num = ssize / 4;

                        for (n = 0; n < num; n++) {
                            int igen = fget16(f);
                            fget16(f);

                            /* pick start of igen */
                            if (n == ibag_s)
                                igen_s = igen;

                            /* pick end of igen */
                            if (n == ibag_e)
                                igen_e = igen;
                        }

                        break;

                    case CID_igen:

                        num = ssize / 4;

                        /* init layers */
                        for (n = 0; n < MAX_LAYERS; n++) {
                            base_notes[n] = -1;
                            min_notes[n] = 0;
                            max_notes[n] = 127;
                        }

                        for (n = 0; n < num; n++) {
                            int op = fget16(f);
                            int am = fget16(f);

                            if (n >= igen_s && n < igen_e) {

                                if (op == 58)   /* overridingRootKey */
                                    base_notes[i_layers] = am;

                                if (op == 43) { /* keyRange */
                                    min_notes[i_layers] = am & 0xff;
                                    max_notes[i_layers] = am >> 8;
                                }

                                if (op == 53) { /* sampleID */
                                    sampleids[i_layers++] = am;
                                }
                            }
                        }

                        break;

                    case CID_shdr:
                        /* sample headers */
                        num = ssize / 46;

                        for (n = 0; n < num; n++) {
                            int m;
                            int start, end;
                            int loop_start, loop_end;
                            int sample_rate, base_note;
                            int sample_type;

                            fread(tmp, 20, 1, f);
                            tmp[20] = '\0';

                            start = fget32(f);
                            end = fget32(f);
                            loop_start = fget32(f);
                            loop_end = fget32(f);
                            sample_rate = fget32(f);
                            base_note = fgetc(f);
                            fgetc(f);   /* correction */
                            fget16(f);  /* sample_link */
                            sample_type = fget16(f);

                            for (m = 0; m < i_layers; m++) {
                                struct ss_wave *w;
                                int t;

                                if (sampleids[m] != n)
                                    continue;

                                /* create wave! */
                                if (base_notes[m] != -1)
                                    base_note = base_notes[m];

                                if (verbose >= 2) {
                                    printf("SF2 Sample:\t[%s]\n", tmp);
                                    printf("start/end:\t%d/%d (%d)\n",
                                           start, end, end - start);
                                    printf("loop:\t\t%d/%d\n", loop_start,
                                           loop_end);
                                    printf("rate/stype:\t%d/%d\n",
                                           sample_rate, sample_type);
                                    printf("notes:\t\t%d/%d/%d\n",
                                           min_notes[m], base_note,
                                           max_notes[m]);
                                }

                                w = ss_alloc_wave(end - start, 1,
                                                  sample_rate, -1);

                                w->loop_start = loop_start - start;
                                w->loop_end = loop_end - start;
                                w->base_freq =
                                    ss_note_frequency(base_note);
                                w->min_freq =
                                    ss_note_frequency(min_notes[m]);
                                w->max_freq =
                                    ss_note_frequency(max_notes[m]);

                                w->bits = 16;
                                w->sign = 1;

                                switch (sample_type) {
                                case 2:        /* right */
                                    w->first_channel = 1;
                                    /* fallthrough */
                                case 4:        /* left */
                                    w->skip_channels = 1;
                                    break;
                                }

                                t = ftell(f);

                                fseek(f, sample_offset + (start * 2),
                                      SEEK_SET);

                                ss_prepare_wave(w);
                                load_pcm_wave(f, w);

                                fseek(f, t, SEEK_SET);

                                ss_ins_add_layer(i, w);
                            }
                        }

                        break;
                    }

                    fseek(f, send, SEEK_SET);
                    break;

                case CID_sdta:

                    /* sample info starts here */
                    sample_offset = ftell(f);

                    fseek(f, send, SEEK_SET);
                    break;

                default:
                    /* unknown chunk */
                    fseek(f, cend, SEEK_SET);
                    break;
                }
            }

            break;

        default:
            fseek(f, cend, SEEK_SET);
            break;
        }
    }

    fclose(f);

    if (iname != NULL && ibag_s == -1)
        return -104;

    return 0;
}
