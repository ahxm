/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2005/2011 Angel Ortega <angel@triptico.com>

    ss_outdev.c - Softsynth output devices

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void *d_unsupp(const char *drvname, const char *filename, int freq,
                      int n_channels, int *ssize)
/* unsupported stub function */
{
    return NULL;
}


/** Arts driver **/

#ifdef CONFOPT_ARTS

#include <artsc.h>

struct ss_outdrv_arts
/* control structure */
{
    void (*func) (void *, short int *);
    arts_stream_t arts;
    int size;
};

static void d_arts_func(void *d, short int *frame)
/* writing / close function */
{
    struct ss_outdrv_arts *drv = d;

    if (frame != NULL)
        arts_write(drv->arts, frame, drv->size);
    else
        arts_free();
}

static void *d_arts(const char *drvname, const char *filename, int freq,
                    int n_channels, int *ssize)
/* init function */
{
    static struct ss_outdrv_arts drv;

    if (drvname && strcmp("arts", drvname) != 0)
        return NULL;

    /* filename specified? get out */
    if (filename != NULL)
        return NULL;

    *ssize = sizeof(struct ss_outdrv_arts);
    drv.func = d_arts_func;
    drv.size = n_channels >= 2 ? 2 : 1;

    if (arts_init()
        || (drv.arts =
            arts_play_stream(freq, 16, drv.size, "ahxm")) == NULL)
        return NULL;

    drv.size *= sizeof(short int);

    return &drv;
}


#else                           /* CONFOPT_ARTS */

#define d_arts d_unsupp

#endif                          /* CONFOPT_ARTS */


/** ESD driver **/

#ifdef CONFOPT_ESD

#include <unistd.h>
#include <esd.h>

struct ss_outdrv_esd
/* control structure */
{
    void (*func) (void *, short int *);
    int fd;
    int size;
};

static void d_esd_func(void *d, short int *frame)
/* write / close function */
{
    struct ss_outdrv_esd *drv = d;

    if (frame != NULL)
        write(drv->fd, frame, drv->size);
    else
        close(drv->fd);
}

static void *d_esd(const char *drvname, const char *filename, int freq,
                   int n_channels, int *ssize)
/* init function */
{
    static struct ss_outdrv_esd drv;
    esd_format_t format;

    if (drvname && strcmp("esd", drvname) != 0)
        return NULL;

    *ssize = sizeof(struct ss_outdrv_esd);
    drv.func = d_esd_func;
    drv.size = n_channels >= 2 ? 2 : 1;

    format = ESD_STREAM | ESD_PLAY | ESD_BITS16;
    format |= drv.size == 2 ? ESD_STEREO : ESD_MONO;

    if (esd_open_sound(filename) < 0 ||
        (drv.fd =
         esd_play_stream_fallback(format, freq, NULL, "ahxm")) < 0)
        return NULL;

    drv.size *= sizeof(short int);

    return &drv;
}


#else                           /* CONFOPT_ESD */

#define d_esd d_unsupp

#endif                          /* CONFOPT_ESD */


/** Pulseaudio driver **/

#ifdef CONFOPT_PULSEAUDIO

#include <pulse/simple.h>
#include <pulse/error.h>

struct ss_outdrv_pulse
/* control structure */
{
    void (*func) (void *, short int *);
    pa_simple *pa;
    int size;
};

static void d_pulse_func(void *d, short int *frame)
/* write / close function */
{
    struct ss_outdrv_pulse *drv = d;
    int error;

    if (frame != NULL)
        pa_simple_write(drv->pa, frame, drv->size, &error);
    else {
        pa_simple_drain(drv->pa, &error);
        pa_simple_free(drv->pa);
    }
}

static void *d_pulse(const char *drvname, const char *filename, int freq,
                     int n_channels, int *ssize)
/* init function */
{
    static struct ss_outdrv_pulse drv;
    pa_sample_spec ss;

    if (drvname && strcmp("pulse", drvname) != 0)
        return NULL;

    ss.format = PA_SAMPLE_S16LE;
    ss.rate = freq;
    ss.channels = n_channels;

    *ssize = sizeof(struct ss_outdrv_pulse);
    drv.func = d_pulse_func;
    drv.size = n_channels * sizeof(short int);

    if ((drv.pa = pa_simple_new(filename, "ahxm", PA_STREAM_PLAYBACK,
                                NULL, "ahxm", &ss, NULL, NULL,
                                NULL)) == NULL)
        return NULL;

    return &drv;
}

#else                           /* CONFOPT_PULSEAUDIO */

#define d_pulse d_unsupp

#endif                          /* CONFOPT_PULSEAUDIO */


/** SGI Irix driver **/

#ifdef CONFOPT_SGI

#include <dmedia/audio.h>

struct ss_outdrv_sgi
/* control structure */
{
    void (*func) (void *, short int *);
    ALconfig ac;
    ALport ap;
};

static void d_sgi_func(void *d, short int *frame)
/* write / close function */
{
    struct ss_outdrv_sgi *drv = d;

    if (frame != NULL)
        alWriteFrames(drv->ap, frame, 1);
    else {
        alClosePort(drv->ap);
        alFreeConfig(drv->ac);
    }
}

static void *d_sgi(const char *drvname, const char *filename, int freq,
                   int n_channels, int *ssize)
/* init function */
{
    static struct ss_outdrv_sgi drv;
    ALpv p[2];

    if (drvname && strcmp("sgi", drvname) != 0)
        return NULL;

    /* filename specified? get out */
    if (filename != NULL)
        return NULL;

    *ssize = sizeof(struct ss_outdrv_sgi);
    drv.func = d_sgi_func;

    p[0].param = AL_MASTER_CLOCK;
    p[0].value.i = AL_CRYSTAL_MCLK_TYPE;
    p[1].param = AL_RATE;
    p[1].value.ll = alDoubleToFixed((double) freq);

    if (!(drv.ac = alNewConfig()))
        return NULL;

    if (alSetChannels(drv.ac, n_channels) < 0 ||
        alSetWidth(drv.ac, AL_SAMPLE_16) < 0 ||
        alSetSampFmt(drv.ac, AL_SAMPFMT_TWOSCOMP) < 0 ||
        alSetQueueSize(drv.ac, 2048) < 0 ||
        alSetDevice(drv.ac, AL_DEFAULT_OUTPUT) < 0 ||
        alSetParams(alGetDevice(drv.ac), p, 2) < 0 ||
        !(drv.ap = alOpenPort("ahxm", "w", drv.ac))) {
        alFreeConfig(drv.ac);
        return NULL;
    }

    return &drv;
}

#else                           /* CONFOPT_SGI */

#define d_sgi d_unsupp

#endif                          /* CONFOPT_SGI */


/** Linux OSS driver **/

#ifdef CONFOPT_LINUX_OSS

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/soundcard.h>
#include <unistd.h>

struct ss_outdrv_oss
/* control structure */
{
    void (*func) (void *, short int *);
    int fd;
    int size;
};

static int is_little_endian(void)
/* is this machine little endian? */
{
    short s = 1;
    unsigned char *c = (unsigned char *) &s;

    return *c == 1;
}

static void d_oss_func(void *d, short int *frame)
/* write / close function */
{
    struct ss_outdrv_oss *drv = d;

    if (frame != NULL)
        write(drv->fd, frame, drv->size);
    else
        close(drv->fd);
}


static void *d_oss(const char *drvname, const char *filename, int freq,
                   int n_channels, int *ssize)
/* init function */
{
    static struct ss_outdrv_oss drv;
    int fr, st, fm;

    if (drvname && strcmp("oss", drvname) != 0)
        return NULL;

    if (filename == NULL)
        filename = "/dev/dsp";

    fr = (2 << 16) | 12;
    st = n_channels >= 2 ? 1 : 0;
    fm = is_little_endian()? AFMT_S16_LE : AFMT_S16_BE;

    *ssize = sizeof(struct ss_outdrv_oss);
    drv.func = d_oss_func;
    drv.size = n_channels >= 2 ? 2 : 1;

    if ((drv.fd = open(filename, O_WRONLY)) < 0)
        return NULL;

    if (ioctl(drv.fd, SNDCTL_DSP_SETFRAGMENT, &fr) < 0 ||
        ioctl(drv.fd, SNDCTL_DSP_RESET, 0) < 0 ||
        ioctl(drv.fd, SNDCTL_DSP_SPEED, &freq) < 0 ||
        ioctl(drv.fd, SNDCTL_DSP_STEREO, &st) < 0 ||
        ioctl(drv.fd, SNDCTL_DSP_SETFMT, &fm) < 0) {
        close(drv.fd);
        return NULL;
    }

    drv.size *= sizeof(short int);

    return &drv;
}

#else                           /* CONFOPT_LINUX_OSS */

#define d_oss d_unsupp

#endif                          /* CONFOPT_LINUX_OSS */


/** WAV file driver **/

struct ss_outdrv_wav
/* control structure */
{
    void (*func) (void *, short int *);
    FILE *fd;
    int size;
    int n_frames;
};

static void fput16(short int i, FILE * f)
/* writes a 16 bit integer, in any machine order */
{
    fputc(i & 0x00ff, f);
    fputc((i & 0xff00) >> 8, f);
}

static void fput32(int i, FILE * f)
/* writes a 32 bit integer, in any machine order */
{
    fputc(i & 0x000000ff, f);
    fputc((i & 0x0000ff00) >> 8, f);
    fputc((i & 0x00ff0000) >> 16, f);
    fputc((i & 0xff000000) >> 24, f);
}

static void d_wav_func(void *d, short int *frame)
/* write / close function */
{
    struct ss_outdrv_wav *drv = d;

    if (frame != NULL) {
        int n;

        for (n = 0; n < drv->size; n++)
            fput16(frame[n], drv->fd);

        drv->n_frames++;
    }
    else {
        /* rewind file and write total size */
        fseek(drv->fd, 4, SEEK_SET);
        fput32((drv->n_frames * drv->size * 2) + 36, drv->fd);

        fseek(drv->fd, 40, SEEK_SET);
        fput32((drv->n_frames * drv->size * 2), drv->fd);

        fclose(drv->fd);
    }
}

static void *d_wav(const char *drvname, const char *filename, int freq,
                   int n_channels, int *ssize)
/* init function */
{
    static struct ss_outdrv_wav drv;

    if (drvname && strcmp("wav", drvname) != 0)
        return NULL;

    if (filename == NULL)
        filename = "output.wav";

    *ssize = sizeof(struct ss_outdrv_wav);
    drv.func = d_wav_func;
    drv.size = n_channels;
    drv.n_frames = 0;

    if ((drv.fd = fopen(filename, "wb")) == NULL)
        return 0;

    /* write wav header */

    fwrite("RIFF", 1, 4, drv.fd);
    fput32(36, drv.fd);         /* first checkpoint (offset: 4) */
    fwrite("WAVE", 1, 4, drv.fd);
    fwrite("fmt ", 1, 4, drv.fd);
    fput32(16, drv.fd);         /* chunk size */
    fput16(1, drv.fd);          /* 1: uncompressed PCM */
    fput16(n_channels, drv.fd); /* # of channels */
    fput32(freq, drv.fd);       /* sample rate */
    fput32(freq * n_channels * 2, drv.fd);      /* bytes per second */
    fput16(n_channels * 2, drv.fd);     /* 'block align' */
    fput16(16, drv.fd);         /* 16 bits per sample */
    fwrite("data", 1, 4, drv.fd);
    fput32(0, drv.fd);          /* second checkpoint (offset: 40) */

    return &drv;
}


/** Pthread support **/

#ifdef CONFOPT_PTHREADS

#include <pthread.h>

static pthread_t consumer;
static pthread_mutex_t mutex;
static pthread_cond_t cond;

/* the real function inside the driver */
static void (*real_drv_func) (void *, short int *) = NULL;

/* number of ints in frame_cpy */
static int frame_cpy_n = 0;

/* a copy of the frame */
static short int *frame_cpy = NULL;

static void *outdev_thread(void *driver)
/* pthread 'consumer' function */
{
    pthread_mutex_lock(&mutex);

    for (;;) {
        /* wait for data available */
        pthread_cond_wait(&cond, &mutex);

        /* call the real function */
        real_drv_func(driver, frame_cpy);

        /* no more? */
        if (frame_cpy == NULL)
            break;
    }

    pthread_mutex_unlock(&mutex);
    return NULL;
}


static void outdev_producer_wrapper(void *d, short int *frame)
/* wrapper 'producer' function */
{
    int n;

    pthread_mutex_lock(&mutex);

    if (frame == NULL) {
        /* close */
        free(frame_cpy);
        frame_cpy = NULL;
        frame_cpy_n = 0;
    }
    else {
        /* copy the frame */
        for (n = 0; n < frame_cpy_n; n++)
            frame_cpy[n] = frame[n];
    }

    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex);
}


static void thread_setup(void *d, int n_channels)
{
    void (**drv) (void *, short int *) = d;

    /* create the buffer */
    frame_cpy_n = n_channels;
    frame_cpy = malloc(frame_cpy_n * sizeof(short int));

    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cond, NULL);

    /* 'patch' the driver */
    real_drv_func = *drv;
    *drv = outdev_producer_wrapper;

    /* launch the thread */
    pthread_create(&consumer, NULL, outdev_thread, d);
}


#else                           /* CONFOPT_PTHREADS */

void thread_setup(void *d, int n)
{
    /* do nothing */
}

#endif                          /* CONFOPT_PTHREADS */


/** win32 waveout support **/

#ifdef CONFOPT_WIN32

#include <windows.h>
#include <mmsystem.h>

#define WAVEOUT_BUF_SIZE    4096 * 4
#define WAVEOUT_N_BUFFERS   8

struct _wob {
    WAVEHDR header;
    unsigned char buf[WAVEOUT_BUF_SIZE];
    int offset;
};

struct ss_outdrv_win32
/* control structure */
{
    void (*func) (void *, short int *);
    HWAVEOUT hwaveout;
    struct _wob wob[WAVEOUT_N_BUFFERS];
    int wob_i;
    int size;
};

static void d_win32_func(void *d, short int *frame)
{
    struct ss_outdrv_win32 *drv = d;
    struct _wob *b = &drv->wob[drv->wob_i];
    int w = 1;

    if (frame != NULL) {
        /* fill buffer */
        memcpy(b->buf + b->offset, frame, drv->size);
        b->offset += drv->size;

        /* still not full? do not write then */
        if (b->offset + drv->size < WAVEOUT_BUF_SIZE)
            w = 0;
    }

    if (w) {
        /* clean previous buffer, if needed */
        if (b->header.lpData)
            waveOutUnprepareHeader(drv->hwaveout, &b->header,
                                   sizeof(WAVEHDR));

        /* prepare new buffer */
        b->header.dwBufferLength = b->offset;
        b->header.lpData = (LPSTR) b->buf;

        /* send it */
        waveOutPrepareHeader(drv->hwaveout, &b->header, sizeof(WAVEHDR));
        waveOutWrite(drv->hwaveout, &b->header, sizeof(WAVEHDR));

        if (frame != NULL) {
            /* next buffer */
            if (++drv->wob_i == WAVEOUT_N_BUFFERS)
                drv->wob_i = 0;

            b = &drv->wob[drv->wob_i];

            /* reset */
            b->offset = 0;
        }

        /* wait until buffer is done */
        while (b->header.lpData != NULL
               && !(b->header.dwFlags & WHDR_DONE))
            Sleep(1);
    }
}


static void *d_win32(const char *drvname, const char *filename, int freq,
                     int n_channels, int *ssize)
{
    static struct ss_outdrv_win32 drv;
    WAVEFORMATEX wfx;

    if (drvname && strcmp("win32", drvname) != 0)
        return NULL;

    memset(&drv, '\0', sizeof(drv));

    wfx.nSamplesPerSec = freq;
    wfx.wBitsPerSample = 16;
    wfx.nChannels = n_channels;
    wfx.cbSize = 0;
    wfx.wFormatTag = WAVE_FORMAT_PCM;
    wfx.nBlockAlign = (wfx.wBitsPerSample >> 3) * wfx.nChannels;
    wfx.nAvgBytesPerSec = wfx.nBlockAlign * wfx.nSamplesPerSec;

    if (waveOutOpen(&drv.hwaveout, WAVE_MAPPER,
                    &wfx, 0, 0, CALLBACK_NULL) != MMSYSERR_NOERROR)
        return NULL;

    drv.func = d_win32_func;
    drv.size = n_channels * sizeof(short int);
    drv.wob_i = 0;

    *ssize = sizeof(struct ss_outdrv_win32);

    return &drv;
}


#else                           /* CONFOPT_WIN32 */

#define d_win32 d_unsupp

#endif                          /* CONFOPT_WIN32 */


/** Interface **/

void *ss_outdev_open(const char *drvname, const char *file, int freq,
                     int n_channels)
{
    int size;
    void *drv;
    void *ret = NULL;

    if ((drv = d_arts(drvname, file, freq, n_channels, &size)) != NULL ||
        (drv = d_esd(drvname, file, freq, n_channels, &size)) != NULL ||
        (drv = d_pulse(drvname, file, freq, n_channels, &size)) != NULL ||
        (drv = d_sgi(drvname, file, freq, n_channels, &size)) != NULL ||
        (drv = d_oss(drvname, file, freq, n_channels, &size)) != NULL ||
        (drv = d_win32(drvname, file, freq, n_channels, &size)) != NULL ||
        (drv = d_wav(drvname, file, freq, n_channels, &size)) != NULL) {
        ret = malloc(size);
        memcpy(ret, drv, size);
    }

    thread_setup(ret, n_channels);

    return ret;
}


void ss_outdev_write(void *drv, short int *frame)
{
    void (**func) (void *, short int *) = drv;

    (*func) (drv, frame);
}


void *ss_outdev_close(void *drv)
{
    if (drv != NULL) {
        ss_outdev_write(drv, NULL);
        free(drv);
    }

    return NULL;
}
