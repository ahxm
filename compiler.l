%{
/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    compiler.l - Scripting language [F]lex lexer

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "y.tab.h"

#include "ahxm.h"

void yyerror(const char *s);
int yy_input_for_flex(char *buf, int max);

/* the notes */
static const char *ascii_notes = "ccddeffggaab";

/* redefinition of input function for GNU Flex */
#undef YY_INPUT
#define YY_INPUT(b,r,m) (r = yy_input_for_flex(b,m))

struct code_stack {
    const char *code;           /* code string */
    int offset;                 /* current offset */
};

static struct code_stack *code_stack = NULL;
static int code_stack_i = -1;

/* dynamic string manipulation macros */
#ifndef ds_init
struct ds {
    char *d;
    int p;
    int s;
};
#define ds_init(x) do { x.d = (char *)0; x.p = x.s = 0; } while(0)
#define ds_rewind(x) x.p = 0;
#define ds_free(x) do { if (x.d) free(x.d); ds_init(x); } while(0)
#define ds_redim(x) do { if (x.p >= x.s) x.d = realloc(x.d, x.s += 32); } while(0)
#define ds_poke(x,c) do { ds_redim(x); x.d[x.p++] = c; } while(0)
#define ds_pokes(x,t) do { char *p = t; while (*p) ds_poke(x, *p++); } while(0)
#endif                          /* ds_init */

/* block dynstring */
static struct ds ds_blk;

/* count of parentheses */
static int ds_blk_i;

/* line number */
int yyline = 0;

%}

DIGIT		[0-9]
P_INTEGER	{DIGIT}+
S_INTEGER	[-+]{P_INTEGER}
P_REAL		{DIGIT}*[\.]?{DIGIT}+
S_REAL		[-+]{P_REAL}

NOTE_P		[a-g]
NOTE_T3		\/3
NOTE_T5		\/5

MARKNAME	[-a-zA-Z0-9_]+
NEW_MARK	\^{MARKNAME}
GOTO_MARK	@{MARKNAME}
ASSERT_MARK	!{MARKNAME}

BLOCKNAME	[-a-zA-Z0-9_#&]+

WSPACE		[ \t]+

ALTSTR		A[-&#]*

%x REM XC REMXC BLK

%%

{P_INTEGER}		{
				/* integers without sign */
				yylval.i = atoi(yytext);
				return P_INTEGER;
			}
{S_INTEGER}		{
				/* signed integers */
				yylval.i = atoi(yytext);
				return S_INTEGER;
			}
{P_REAL}		{
				/* real numbers without sign */
				yylval.d = atof(yytext);
				return P_REAL;
			}
{S_REAL}		{
				/* signel real numbers */
				yylval.d = atof(yytext);
				return S_REAL;
			}

{NOTE_P}		{
				/* note pitch */
				yylval.i = strchr(ascii_notes, *yytext) - ascii_notes;
				return NOTE_P;
			}
{NOTE_T3}		{ return NOTE_T3; }
{NOTE_T5}		{ return NOTE_T5; }

{NEW_MARK}		{
				/* create new mark */
				yylval.p = yytext + 1;
				return NEW_MARK;
			}
{GOTO_MARK}		{
				/* go to mark */
				yylval.p = yytext + 1;
				return GOTO_MARK;
			}
{ASSERT_MARK}		{
				/* assert mark */
				yylval.p = yytext + 1;
				return ASSERT_MARK;
			}

{ALTSTR}		{
				/* alteration string */
				yylval.p = yytext + 1;
				return ALTSTR;
			}

{WSPACE}		{ ; /* ignore blanks */ }

\n			{ yyline++; }

\/\*			{ BEGIN REM; /* C-like comments */ }
<REM>\*\/		{ BEGIN 0; }
<REM>\n			{ yyline++; }
<REM>.			{ ; /* drop anything inside a comment */ }

\{			{ BEGIN XC; /* start of extended commands */ }
<XC>\"[^\"]*\"		{
				/* double-quoted string */
				yytext[yyleng - 1] = '\0';
				yylval.p = strdup(yytext + 1);
				return XC_STR;
			}
<XC>\'[^\']*\'		{
				/* single-quoted string */
				yytext[yyleng - 1] = '\0';
				yylval.p = strdup(yytext + 1);
				return XC_STR;
			}
<XC>{P_INTEGER}		{
				yylval.i = atoi(yytext);
				return P_INTEGER;
			}
<XC>{S_INTEGER}		{
				yylval.i = atoi(yytext);
				return S_INTEGER;
			}
<XC>{P_REAL}		{
				yylval.d = atof(yytext);
				return P_REAL;
			}
<XC>{S_REAL}		{
				yylval.d = atof(yytext);
				return S_REAL;
			}
<XC>{P_REAL}s		{
				/* frame count, in seconds */
				yytext[yyleng - 1] = '\0';
				yylval.d = atof(yytext) * 1000;
				return XC_MSECS;
			}
<XC>{P_REAL}ms		{
				/* frame count, in milliseconds */
				yytext[yyleng - 2] = '\0';
				yylval.d = atof(yytext);
				return XC_MSECS;
			}
<XC>{NOTE_P}[\#\&]?{P_INTEGER} {
				char *ptr = yytext;

				/* process note */
				yylval.i = strchr(ascii_notes, *ptr) - ascii_notes;
				ptr++;

				/* process optional sharps or flats */
				if (*ptr == '#') {
					yylval.i++;
					ptr++;
				}
				else
				if (*ptr == '&') {
					yylval.i--;
					ptr++;
				}

				/* process octave */
				yylval.i += atoi(ptr) * 12;

				return XC_ABSNOTE;
			}

<XC>wav			{ return SS_WAV; }
<XC>pat			{ return SS_PAT; }
<XC>sf2			{ return SS_SF2; }
<XC>sustain		{ return SS_SUSTAIN; }
<XC>attack		{ return SS_ATTACK; }
<XC>vibrato		{ return SS_VIBRATO; }
<XC>portamento		{ return SS_PORTAMENTO; }
<XC>channel		{ return SS_CHANNEL; }
<XC>vol			{ return SS_VOL; }
<XC>master_volume	{ return SS_MASTER_VOL; }

<XC>delay		{ return SS_EFF_DELAY; }
<XC>echo		{ return SS_EFF_ECHO; }
<XC>comb		{ return SS_EFF_COMB; }
<XC>allpass		{ return SS_EFF_ALLPASS; }
<XC>flanger		{ return SS_EFF_FLANGER; }
<XC>wobble		{ return SS_EFF_WOBBLE; }
<XC>square_wobble	{ return SS_EFF_SQWOBBLE; }
<XC>half_wobble		{ return SS_EFF_HFWOBBLE; }
<XC>fader		{ return SS_EFF_FADER; }
<XC>reverb		{ return SS_EFF_REVERB; }
<XC>foldback		{ return SS_EFF_FOLDBACK; }
<XC>atan		{ return SS_EFF_ATAN; }
<XC>distort		{ return SS_EFF_DISTORT; }
<XC>overdrive		{ return SS_EFF_OVERDRIVE; }
<XC>off			{ return SS_EFF_OFF; }

<XC>pitch_stretch	{ return SS_PITCH_STRETCH; }
<XC>time_stretch	{ return SS_TIME_STRETCH; }
<XC>print_wave_tempo	{ return SS_PRINT_WAVE_TEMPO; }

<XC>song_info		{ return SONG_INFO; }

<XC>midi_channel	{ return MIDI_CHANNEL; }
<XC>midi_program	{ return MIDI_PROGRAM; }
<XC>midi_generic	{ return MIDI_GENERIC; }

<XC>\/\*		{ BEGIN REMXC; /* C-like comments inside XC */ }
<REMXC>\*\/		{ BEGIN XC; /* jump back to XC processing */ }
<REMXC>\n		{ yyline++; }
<REMXC>.		{ ; }

<XC>\}			{ BEGIN 0; }
<XC>{WSPACE}		{ ; /* ignore blanks */ }
<XC>\n			{ yyline++; }
<XC>.			{ return *yytext; }

\(			{
				/* start of block */
				ds_init(ds_blk);
				ds_blk_i = 1;
				BEGIN BLK;
			}
<BLK>\(			{ ds_blk_i++; ds_poke(ds_blk, *yytext); }
<BLK>\)			{
				/* one parentheses less */
				ds_blk_i--;

				/* ok with block? */
				if (ds_blk_i == 0) {
					ds_poke(ds_blk, '\0');
					yylval.p = ds_blk.d;

					BEGIN 0;
					return BLOCK;
				}
				else
					ds_poke(ds_blk, ')');
			}
<BLK>\n			{ ds_poke(ds_blk, ' '); yyline++; }
<BLK>.			{ ds_poke(ds_blk, *yytext); }

=[ \t\n]*{BLOCKNAME}	{
				/* block assignation */
				yylval.p = yytext + 1;

				/* skip (possible) spaces between
				   the = and the blockname; this lex
				   rule is written this way to avoid
				   having a global match for {BLOCKNAME},
				   that could swallow notes and such,
				   being mismatched as blocknames */
				while (*yylval.p == ' ' ||
				      *yylval.p == '\n' ||
				      *yylval.p == '\t')
					yylval.p++;

				return BLK_ASSIGN;
			}
\${BLOCKNAME}		{
				/* block insertion */
				yylval.p = yytext + 1;
				return BLK_INSERT;
			}
`.+`			{
				/* file inclusion */
				yytext[yyleng - 1] = '\0';
				yylval.p = yytext + 1;
				return FILE_INSERT;
			}
L[^ \t\n]+		{
				/* add libpath */
				yylval.p = yytext + 1;
				return LIBPATH_ADD;
			}

.			{ return *yytext; }

%%

int yywrap(void)
{
    return 1;
}

char *ds_load(const char *file)
{
    struct ds s;
    int c;
    FILE *f;

    if ((f = libpath_fopen(file, "rb")) == NULL)
        return (NULL);

    ds_init(s);

    while ((c = fgetc(f)) != EOF)
        ds_poke(s, c);

    ds_poke(s, '\0');
    fclose(f);

    return s.d;
}


int push_code(const char *code)
{
    struct code_stack *cs;

    code_stack_i++;
    GROW(code_stack, code_stack_i, struct code_stack);

    cs = &code_stack[code_stack_i];

    cs->code = code;
    cs->offset = 0;

    return 1;
}


int push_code_from_file(const char *file)
{
    char *c;

    if ((c = ds_load(file)) == NULL)
        return 0;

    push_code(c);
    return 1;
}


int code_getchar(void)
{
    struct code_stack *cs;
    int c = '\0';

    while (c == '\0' && code_stack_i > -1) {
        /* get current char */
        cs = &code_stack[code_stack_i];
        c = cs->code[cs->offset++];

        /* end of code? */
        if (c == '\0') {
            /* free */
            free((char *) cs->code);

            /* move to previous */
            code_stack_i--;

            /* in any case, return a separator */
            c = ' ';
        }
    }

    return c;
}


int yy_input_for_flex(char *buf, int max)
{
    buf[0] = code_getchar();

    return buf[0] == '\0' ? 0 : 1;
}
