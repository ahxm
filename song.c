/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    song.c - Device-independent song event stream management

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ahxm.h"

/** data **/

/* the song event stream */
struct song_ev *song = NULL;

/* number of song events */
int n_song_ev = 0;

/* number of tracks in song */
int n_song_tracks = 0;


/** code **/

/**
 * add_event - Adds an event to an event list.
 * @song: A pointer to an event list (dynamically allocated)
 * @count: A pointer to an integer (number of elements)
 *
 * Increments the @song event list to fit @count number of
 * elements, probably reallocating its memory.
 *
 * Returns a pointer to the new element.
 */
struct song_ev *add_event(struct song_ev **song, int *count)
{
    struct song_ev *r;

    GROW(*song, *count, struct song_ev);

    r = *song + *count;
    memset(r, '\0', sizeof(struct song_ev));

    (*count)++;

    return r;
}


/**
 * copy_event - Appends an event to an event list.
 * @song: the event list
 * @count: a pointer to the number of elements
 * @e: the event to be copied
 *
 * Appends the @e event to the @song event list.
 */
struct song_ev *copy_event(struct song_ev **song, int *count,
                           const struct song_ev *e)
{
    struct song_ev *r = add_event(song, count);

    memcpy(r, e, sizeof(struct song_ev));

    return r;
}


/**
 * song_clear - Clears the song stream
 *
 * Clears the song stream.
 */
void song_clear(void)
{
    if (song != NULL) {
        free(song);
        song = NULL;
    }

    n_song_ev = 0;
}


static char *event_name(song_ev_type t)
{
    char *ptr = "UNKNOWN";

    switch (t) {
    case SONG_EV_TEMPO:
        ptr = "TEMPO--";
        break;

    case SONG_EV_METER:
        ptr = "METER--";
        break;

    case SONG_EV_MEASURE:
        ptr = "MEASURE";
        break;

    case SONG_EV_SS_SUSTAIN:
        ptr = "SUSTAIN";
        break;

    case SONG_EV_SS_ATTACK:
        ptr = "ATTACK-";
        break;

    case SONG_EV_SS_VIBRATO:
        ptr = "VIBRATO";
        break;

    case SONG_EV_SS_PORTAMENTO:
        ptr = "PORTMNT";
        break;

    case SONG_EV_SS_CHANNEL:
        ptr = "CHANNEL";
        break;

    case SONG_EV_SS_WAV:
        ptr = "WAV----";
        break;

    case SONG_EV_SS_PAT:
        ptr = "PAT----";
        break;

    case SONG_EV_SS_SF2:
        ptr = "SF2----";
        break;

    case SONG_EV_SS_EFF_OFF:
        ptr = "OFF----";
        break;

    case SONG_EV_SS_EFF_DELAY:
        ptr = "DELAY--";
        break;

    case SONG_EV_SS_EFF_ECHO:
        ptr = "ECHO---";
        break;

    case SONG_EV_SS_EFF_COMB:
        ptr = "COMB---";
        break;

    case SONG_EV_SS_EFF_ALLPASS:
        ptr = "ALLPASS";
        break;

    case SONG_EV_SS_EFF_FLANGER:
        ptr = "FLANGER";
        break;

    case SONG_EV_SS_EFF_WOBBLE:
        ptr = "WOBBLE-";
        break;

    case SONG_EV_SS_EFF_SQWOBBLE:
        ptr = "SQWOBBL";
        break;

    case SONG_EV_SS_EFF_HFWOBBLE:
        ptr = "HFWOBBL";
        break;

    case SONG_EV_SS_EFF_FADER:
        ptr = "FADER--";
        break;

    case SONG_EV_SS_EFF_REVERB:
        ptr = "REVERB-";
        break;

    case SONG_EV_SS_EFF_FOLDBACK:
        ptr = "FOLDBCK";
        break;

    case SONG_EV_SS_EFF_ATAN:
        ptr = "ATAN---";
        break;

    case SONG_EV_SS_EFF_DISTORT:
        ptr = "DISTORT";
        break;

    case SONG_EV_SS_EFF_OVERDRIVE:
        ptr = "OVERDRV";
        break;

    case SONG_EV_MIDI_CHANNEL:
        ptr = "MIDICHN";
        break;

    case SONG_EV_MIDI_PROGRAM:
        ptr = "MIDIPRG";
        break;

    case SONG_EV_BACK:
        ptr = "BACK---";
        break;

    case SONG_EV_NOTE_OFF:
        ptr = "NOTEOFF";
        break;

    case SONG_EV_NOTE:
        ptr = "NOTE---";
        break;

    case SONG_EV_SS_PITCH_STRETCH:
        ptr = "PTCHSTR";
        break;

    case SONG_EV_SS_PRINT_WAVE_TEMPO:
        ptr = "PRNTMPO";
        break;

    case SONG_EV_SS_MASTER_VOLUME:
        ptr = "MASTRVL";
        break;

    case SONG_EV_SONG_INFO:
        ptr = "SONGNFO";
        break;

    case SONG_EV_EOT:
        ptr = "EOT----";
        break;

    case SONG_EV_END:
        ptr = "END----";
        break;

    case SONG_EV_NOP:
        ptr = "NOP----";
        break;
    }

    return ptr;
}


void dump_song_event(const struct song_ev *e)
{
    if (e->frame)
        printf("FRM: %8d ", e->frame);

    if (e->msecs)
        printf("MS: %8d ", e->msecs);

    printf("%7.3lf %7s TRK: %2d ID: %4d",
           e->time, event_name(e->type), e->trk_id, e->event_id);

    if (e->value)
        printf(" VAL: %3d", e->value);

    if (e->note_id)
        printf(" NOTE: %3d", e->note_id);

    if (e->min || e->max)
        printf(" [%d,%d]", e->min, e->max);

    printf(" CHN: %d", e->channel);

    if (e->skip_channels)
        printf(" SKIP: %d", e->skip_channels);

    if (e->vol > 0.0)
        printf(" VOL: %5.3lf", e->vol);

    if (e->amount > 0.0)
        printf(" AMOUNT: %7.3lf", e->amount);

    if (e->len > 0.0)
        printf(" LEN: %7.3lf", e->len);

    if (e->name != NULL)
        printf(" NAME: '%s'", e->name);

    if (e->str2 != NULL)
        printf(" STR2: '%s'", e->str2);

    printf("\n");
}


void dump_song_events(const struct song_ev *song, int n_song_ev)
{
    const struct song_ev *e;
    int n;

    for (n = 0, e = song; n < n_song_ev; n++, e++)
        dump_song_event(e);

    printf("\n");
}


static int time_type_eventid_cmp(const void *v1, const void *v2)
/* sorts events by time, then type, then event_id */
{
    struct song_ev *e1;
    struct song_ev *e2;
    int ret;

    e1 = (struct song_ev *) v1;
    e2 = (struct song_ev *) v2;

    ret = (int) ((e1->time * 10000.0) - (e2->time * 10000.0));

    /* same time? order by type of event */
    if (ret == 0)
        ret = e1->type - e2->type;

    /* same time and same event? order by event id */
    if (ret == 0)
        ret = e1->event_id - e2->event_id;

    return ret;
}


static void count_tracks(void)
/* sets n_song_tracks */
{
    int n;

    n_song_tracks = 0;

    for (n = 0; n < n_song_ev; n++) {
        struct song_ev *e;

        e = &song[n];

        if (n_song_tracks < e->trk_id + 1)
            n_song_tracks = e->trk_id + 1;
    }
}


static void add_eot_events(void)
/* travels all events and adds EOT ones */
{
    int n, m;

    for (n = 0; n < n_song_tracks; n++) {
        struct song_ev *e;
        double t = -1;

        for (m = 0; m < n_song_ev; m++) {
            e = &song[m];

            /* if this is the track we're looking for
               and this event time is bigger, store it */
            if (e->trk_id == n && e->time > t)
                t = e->time;
        }

        /* now t has the biggest time; add an EOT event with t */
        e = add_event(&song, &n_song_ev);

        e->type = SONG_EV_EOT;
        e->time = t;
        e->trk_id = n;
        e->event_id = n_song_ev - 1;
    }
}


/**
 * mute_tracks - Converts track events to NOPs.
 * @trk_id: Track to be muted. If negative, all buts this will be muted
 *
 * Converts events to NOP. If @trk_id is positive, all events for that
 * track id will be converted to NOP; if it's negative, all events BUT
 * for the ones in that track will be muted.
 */
void mute_tracks(int trk_id)
{
    int n;

    for (n = 0; n < n_song_ev; n++) {
        struct song_ev *e;

        e = &song[n];

        /* try only 'real' tracks */
        if (e->trk_id >= 0) {
            if (trk_id > 0) {
                if (e->trk_id == trk_id)
                    e->type = SONG_EV_NOP;
            }
            else {
                if (e->trk_id != -trk_id)
                    e->type = SONG_EV_NOP;
            }
        }
    }
}


/**
 * song_sort - Sorts the song stream
 *
 * Sorts the song stream.
 */
void song_sort(void)
{
    count_tracks();
    add_eot_events();

    /* sorts by time, type and event_id */
    qsort(song, n_song_ev, sizeof(struct song_ev), time_type_eventid_cmp);

    if (trace) {
        printf("\n** GENERIC SONG EVENT DUMP **\n\n");
        dump_song_events(song, n_song_ev);
    }
}


/**
 * song_test_measure_boundary - Does a measure boundary check
 * @ev_time: event time
 * @num: meter numerator
 * @den: meter denominator
 *
 * Does a measure boundary check. Returns 0 if the event time falls
 * exactly between two measures, or nonzero otherwise.
 */
int song_test_measure_boundary(double ev_time, int num, int den, int line)
{
    int ret;

    if ((ret = ((int) (ev_time * (double) den)) % num))
        printf("Measure boundary check failed in line %d\n", line);

    return ret;
}
