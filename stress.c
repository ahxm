#include <stdio.h>

#include "config.h"
#include "ahxm.h"

int main(int argc, char *argv[])
{
    int n;
    sample_t s;
    struct ss_wave *w;

    printf("load samples/amen1.wav\n");
    w = ss_load_wav_file("samples/amen1.wav", 440, 440, 440, 0, 0, 0, 0);

    printf("load some samples from wave\n");

    for (n = 0; n < 10; n++)
        printf("%lf\n", ss_get_sample(w, 0, (double) n));

    printf("again, load same samples from wave\n");

    for (n = 0; n < 10; n++)
        printf("%lf\n", ss_get_sample(w, 0, (double) n));

    return 0;
}
