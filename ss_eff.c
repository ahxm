/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    ss_eff.c - Software syntesizer's digital effects

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "ahxm.h"


/** helping functions **/

static struct ss_eff *ss_eff_add(struct ss_eff **ec, double size,
                                 sample_t gain,
                                 sample_t(*func) (struct ss_eff *,
                                                  sample_t))
/* adds an effect to the ec chain */
{
    struct ss_eff *e;

    /* convert to frames */
    size = MS2F(size);

    /* create new structure and reset */
    if ((e = malloc(sizeof(struct ss_eff))) == NULL)
        return NULL;

    memset(e, '\0', sizeof(struct ss_eff));

    /* enqueue */
    if (*ec == NULL)
        *ec = e;
    else {
        struct ss_eff *t;

        /* loop to add e to the end */
        for (t = *ec; t->next != NULL; t = t->next);

        t->next = e;
    }

    /* add buffer */
    if (size > 0) {
        e->wave = ss_alloc_wave((int) size, 1, ss_frequency, -1);
        ss_prepare_wave(e->wave);
    }

    e->gain = gain;
    e->func = func;

    return e;
}


static void ss_eff_set_lfo(struct ss_eff *e, double freq, double phase,
                           double depth)
/* sets an alfo */
{
    e->lfo = phase * 6.28;
    e->lfo_inc = (freq * 6.28) / (double) ss_frequency;
    e->lfo_depth = MS2F(depth);
}


static double ss_eff_lfo(struct ss_eff *e)
/* processes an lfo */
{
    double r = sin(e->lfo);
    e->lfo += e->lfo_inc;

    return r;
}


/**
 * ss_eff_process - Processes a chain of digital effects
 * @e: the effect chain
 * @s: input sample
 *
 * Processes a chain of digital effects, taking each one as input
 * the output of the previous one.
 */
sample_t ss_eff_process(struct ss_eff * e, sample_t s)
{
    while (e != NULL) {
        /* filter */
        s = e->func(e, s);

        /* increment cursor */
        if (e->wave != NULL) {
            if (++e->cursor >= e->wave->size)
                e->cursor = 0;
        }

        /* move to next */
        e = e->next;
    }

    return s;
}


/**
 * ss_eff_off - Destroys a chain of digital effects
 * @ec: the effect chain
 *
 * Destroys a chain of digital effects.
 */
void ss_eff_off(struct ss_eff **ec)
{
    while (*ec != NULL) {
        struct ss_eff *e = (*ec)->next;

        /* free the buffer, if any */
        if ((*ec)->wave != NULL)
            ss_free_wave((*ec)->wave);

        /* free the effect itself */
        free(*ec);

        /* move to next */
        *ec = e;
    }
}


static sample_t *eff_cursor_ptr(struct ss_eff *e)
/* returns a pointer to the current sample */
{
    sample_t *wave = e->wave->wave[0];
    return wave + (int) e->cursor;
}


/** Effect: delay **/

static sample_t func_delay(struct ss_eff *e, sample_t input)
{
    sample_t *p;
    sample_t s;

    p = eff_cursor_ptr(e);
    s = *p;
    *p = input;

    return s;
}


/**
 * ss_eff_delay - Adds a delay effect.
 * @ec: the effect chain
 * @size: delay in milliseconds
 *
 * Adds a delay effect. On output, this effect will simply
 * delay the output of the samples fed to it in @size
 * frames. No further filtering is done.
 */
void ss_eff_delay(struct ss_eff **ec, double size)
{
    ss_eff_add(ec, size, 0, func_delay);
}


/** Effect: echo **/

static sample_t func_echo(struct ss_eff *e, sample_t input)
{
    sample_t *p;
    sample_t s;

    p = eff_cursor_ptr(e);
    s = *p * e->gain;
    *p = input;

    return input + s;
}


/**
 * ss_eff_echo - Adds an echo effect.
 * @ec: the effect chain
 * @size: delay in milliseconds
 * @gain: echo gain
 *
 * Adds an echo effect. Outputs the current sample mixed
 * with the product of the sample sent @size frames ago
 * multiplied by the specified @gain.
 */
void ss_eff_echo(struct ss_eff **ec, double size, sample_t gain)
{
    ss_eff_add(ec, size, gain, func_echo);
}


/** Effect: comb **/

static sample_t func_comb(struct ss_eff *e, sample_t input)
{
    sample_t *p;
    sample_t s;

    p = eff_cursor_ptr(e);
    s = *p;
    *p = input + (s * e->gain);

    return input + s;
}


/**
 * ss_effect_comb - Adds a comb filter.
 * @ec: the effect chain
 * @size: delay in milliseconds
 * @gain: filter gain
 *
 * Adds a comb filter, being @size the number of samples to
 * delay and @gain the feedback output. Comb filters are
 * used in reverbs.
 */
void ss_eff_comb(struct ss_eff **ec, double size, sample_t gain)
{
    ss_eff_add(ec, size, gain, func_comb);
}


/** Effect: allpass **/

static sample_t func_allpass(struct ss_eff *e, sample_t input)
{
    sample_t *p;
    sample_t s, t, u;

    p = eff_cursor_ptr(e);
    t = *p;

    u = input + (t * e->gain);
    s = t - (e->gain * u);

    *p = u;

    return s;
}


/**
 * ss_eff_allpass - Adds an allpass filter.
 * @ec: the effect chain
 * @size: delay in milliseconds
 * @gain: filter gain
 *
 * Adds an allpass filter, being @size the number of samples to
 * delay and @gain the feedback output. Allpass filters are
 * used in reverbs.
 */
void ss_eff_allpass(struct ss_eff **ec, double size, sample_t gain)
{
    ss_eff_add(ec, size, gain, func_allpass);
}


/** Effect: flanger **/

static sample_t func_flanger(struct ss_eff *e, sample_t input)
{
    double c;
    sample_t s;
    sample_t *p;

    c = e->cursor - e->lfo_depth - (e->lfo_depth * ss_eff_lfo(e));

    s = ss_get_sample(e->wave, 0, c) * e->gain;

    p = eff_cursor_ptr(e);
    *p = input;

    return input + s;
}



/**
 * ss_eff_flanger - Adds a flanger effect.
 * @ec: the effect chain
 * @size: delay in milliseconds
 * @gain: output gain
 * @depth: flanger depth in milliseconds
 * @freq: LFO frequency [0..1]
 * @phase: initial phase [0..1]
 *
 * Adds a flanger effect, being @size the number of samples
 * to delay, @gain the output gain, @depth the number of samples
 * the output will be 'flanged' (bigger values mean bigger
 * fluctuations in the final frequency), @freq the frequency of
 * the LFO in Hz and @phase the initial LFO value as a
 * fractional part of a period, being 0 the start of the period,
 * 0.5 half a period and so on. The LFO is sinusoidal.
 */
void ss_eff_flanger(struct ss_eff **ec, double size, sample_t gain,
                    double depth, double freq, double phase)
{
    struct ss_eff *e;

    e = ss_eff_add(ec, size, gain, func_flanger);
    ss_eff_set_lfo(e, freq, phase, depth);
}


/** Effect: wobble **/

static sample_t func_wobble(struct ss_eff *e, sample_t input)
{
    sample_t s;

    s = (input * fabs(ss_eff_lfo(e)) * e->gain) +
        (input * (1.0 - e->gain));

    return s;
}


/**
 * ss_effect_wobble - Adds a wobble effect.
 * @ec: the effect chain
 * @freq: frequency [0..1]
 * @phase: initial phase [0..1]
 * @gain: ammount of effect [0..1]
 *
 * Adds a wobble effect, where the sample amplitudes are
 * multiplied by an LFO, so sound volume wobbles from full
 * volume to silence twice a period. @freq is the LFO frequency
 * in Hz and @phase the initial LFO value as a
 * fractional part of a period, being 0 the start of the period,
 * 0.5 half a period and so on. The LFO is sinusoidal.
 */
void ss_eff_wobble(struct ss_eff **ec, double freq, double phase,
                   sample_t gain)
{
    struct ss_eff *e;

    e = ss_eff_add(ec, 0, gain, func_wobble);
    ss_eff_set_lfo(e, freq, phase, 0);
}


/** Effect: square wave wobble **/

static sample_t func_square_wobble(struct ss_eff *e, sample_t input)
{
    return ss_eff_lfo(e) > 0 ? input : 0;
}


/**
 * ss_eff_square_wobble - Adds a square wave wobble effect.
 * @ec: the effect chain
 * @freq: frequency [0..1]
 * @phase: initial phase [0..1]
 *
 * Adds an effect like the wobble one (see documentation for
 * effect_wobble()), but using a square wave, meaning that input
 * goes unfiltered (full amplitude) for half the period and
 * complete silence the other half.
 */
void ss_eff_square_wobble(struct ss_eff **ec, double freq, double phase)
{
    struct ss_eff *e;

    e = ss_eff_add(ec, 0, 0, func_square_wobble);
    ss_eff_set_lfo(e, freq, phase, 0);
}


/** Effect: half wobble **/

static sample_t func_half_wobble(struct ss_eff *e, sample_t input)
{
    sample_t s;

    s = ss_eff_lfo(e);

    return s > 0.0 ? s * input : 0.0;
}


/**
 * ss_eff_half_wobble - Adds a half wave wobble effect.
 * @ec: the effect chain
 * @freq: frequency [0..1]
 * @phase: initial phase [0..1]
 *
 * Adds an effect like the wobble one (see documentation for
 * effect_wobble()), but returning only the first half of the
 * full period as a wobble and the second as silence.
 */
void ss_eff_half_wobble(struct ss_eff **ec, double freq, double phase)
{
    struct ss_eff *e;

    e = ss_eff_add(ec, 0, 0, func_half_wobble);
    ss_eff_set_lfo(e, freq, phase, 0);
}


/** Effect: fader **/

static sample_t func_fader(struct ss_eff *e, sample_t input)
{
    sample_t s;

    s = input * e->gain;
    e->gain += e->igain;

    if (e->cursor == e->wave->size - 1)
        e->igain = 0;

    return s;
}


/**
 * ss_eff_fader - Adds a fader effect.
 * @ec: the effect chain
 * @size: number of milliseconds the fader will last
 * @initial: initial volume
 * @final: final volume
 *
 * Adds a fader effect. The effect will fade in or out the input
 * volume from @initial to @final during @size samples.
 */
void ss_eff_fader(struct ss_eff **ec, double size, sample_t initial,
                  sample_t final)
{
    struct ss_eff *e;

    e = ss_eff_add(ec, size, initial, func_fader);

    e->igain = (final - initial) / (sample_t) MS2F(size);
}


/** Effect: reverb **/

/**
 * ss_eff_reverb - Adds a simple reverb effect.
 * @ec: the effect chain
 *
 * Adds a simple reverb effect, using a chain of allpass filters.
 */
void ss_eff_reverb(struct ss_eff **ec)
{
    ss_eff_allpass(ec, 20.0, 0.9);
    ss_eff_allpass(ec, 36.0, 0.9);
    ss_eff_allpass(ec, 39.0, 0.9);
}


/** Effect: foldback distortion **/

static sample_t func_foldback(struct ss_eff *e, sample_t input)
{
    sample_t s = input;

    /* http://www.musicdsp.org/archive.php?classid=4#203 */
    if (input > e->gain)
        s = e->gain - (input - e->gain);
    else
    if (input < -e->gain)
        s = -e->gain + (-e->gain - input);

    return s;
}


/**
 * ss_eff_foldback - Adds a foldback distortion effect.
 * @ec: the effect chain
 * @threshold: threshold to apply the folding
 *
 * Adds a foldback distortion effect. All aplitudes above the
 * threshold are folded back.
 */
void ss_eff_foldback(struct ss_eff **ec, sample_t threshold)
{
    ss_eff_add(ec, 0, threshold, func_foldback);
}


/** Effect: atan distortion **/

static sample_t func_atan(struct ss_eff *e, sample_t input)
{
    sample_t s = input;

    /* atan */
    /* http://www.musicdsp.org/showArchiveComment.php?ArchiveID=104 */
    s = atan(s * e->gain) / e->gain;

    return s;
}


/**
 * ss_eff_atan - Adds an 'atan' distortion effect.
 * @ec: the effect chain
 * @gain: amount of effect
 *
 * Adds an 'atan' distortion effect.
 */
void ss_eff_atan(struct ss_eff **ec, sample_t gain)
{
    ss_eff_add(ec, 0, gain, func_atan);
}


/** Effect: distort distortion **/

static sample_t func_distort(struct ss_eff *e, sample_t input)
{
    sample_t s = input;

    /* distort */
    /* http://www.musicdsp.org/showArchiveComment.php?ArchiveID=86 */
    s = s - e->gain * s * s * s;

    return s;
}


/**
 * ss_eff_distort - Adds a 'distort' distortion effect.
 * @ec: the effect chain
 * @gain: amount of effect
 *
 * Adds a 'distort' distortion effect.
 */
void ss_eff_distort(struct ss_eff **ec, sample_t gain)
{
    ss_eff_add(ec, 0, gain, func_distort);
}


/** Effect: overdrive distortion **/

static sample_t func_overdrive(struct ss_eff *e, sample_t input)
{
    sample_t s = input;

    /* overdrive */
    /* http://www.musicdsp.org/archive.php?classid=4#41 */
    s = s * (fabs(s) + e->gain) / (s * s + (e->gain - 1) * fabs(s) + 1);

    return s;
}


/**
 * ss_eff_overdrive - Adds an 'overdrive' distortion effect.
 * @ec: the effect chain
 * @gain: amount of effect
 *
 * Adds an 'overdrive' distortion effect.
 */
void ss_eff_overdrive(struct ss_eff **ec, sample_t gain)
{
    ss_eff_add(ec, 0, gain, func_overdrive);
}
