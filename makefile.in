# -*- Mode: sh

PROJ=ahxm
APPNAME=ahxm
LIB=lib$(PROJ).a

OBJS=support.o sha1.o song.o compiler_l.o compiler_y.o \
	ss_core.o ss_input.o ss_gen.o ss_eff.o ss_ins.o ss_song.o \
	ss_output.o ss_outdev.o midi_song.o main.o

ADD_DOCS=RELEASE_NOTES TODO README
GRUTATXT_DOCS=doc/ahs_overview_i.html \
	doc/ahs_overview_ii.html \
	doc/ahs_appendix_1.html \
	doc/ahxm_index.html
MP_DOCCER_DOCS=doc/ahxm_api.txt
G_AND_MP_DOCS=doc/ahxm_api.html

all: $(BIN) $(DOCS)

DIST_TARGET=/tmp/$(PROJ)-$(VERSION)

##################################################################

version:
	@echo $(VERSION)

.c.o:
	$(CC) $(CFLAGS) `cat config.cflags` -c $<

y.tab.h: compiler.y
	$(YACC) -d compiler.y

y.tab.c: compiler.y
	$(YACC) -d compiler.y

lex.yy.c: compiler.l
	flex compiler.l

compiler_l.o: lex.yy.c y.tab.h
	$(CC) $(CFLAGS) -c lex.yy.c -o compiler_l.o

compiler_y.o: y.tab.c
	$(CC) $(CFLAGS) -c y.tab.c -o compiler_y.o

# library
$(LIB): $(OBJS)
	$(AR) rv $(LIB) $(OBJS)

# binaries
ahxm: $(LIB)
	$(CC) $(CFLAGS) $(LIB) -lm `cat config.ldflags` -o $@

wav: wav.c $(LIB)
	$(CC) $(CFLAGS) wav.c $(LIB) -lm `cat config.ldflags` -o $@

midiin: midiin.c $(LIB)
	$(CC) $(CFLAGS) midiin.c $(LIB) -lm `cat config.ldflags` -o $@

clean:
	rm -f $(BIN) $(LIB) $(OBJS) *.o tags *.tar.gz

realclean: clean
	rm -f y.tab.c y.tab.h lex.yy.c

distclean: realclean
	rm -f config.h config.cflags config.ldflags makefile.opts .config.log Makefile

.SUFFIXES: .txt .html

.txt.html:
	grutatxt < $< > $@

doc/ahxm_api.txt: *.c
	mp_doccer *.c -o doc/ahxm_api -f grutatxt \
		-t "Ann Hell Ex Machina API" \
		-b "This document references version $(VERSION) of the C API." \
		-a 'Angel Ortega - angel@triptico.com'

docsclean:
	rm -f $(MP_DOCCER_DOCS) doc/*.html

distcopy: distclean y.tab.c y.tab.h lex.yy.c
	mkdir -p $(DIST_TARGET) ; \
	tar cf - * | (cd $(DIST_TARGET) ; tar xf -)

dist: distcopy
	(cd /tmp ; tar czf - $(PROJ)-$(VERSION)/* ) > $(PROJ)-$(VERSION).tar.gz ; \
	rm -rf $(DIST_TARGET)

dep:
	gcc -MM *.c > makefile.depend

install:
	install $(APPNAME) $(PREFIX)/bin
	./mkinstalldirs $(PREFIX)/share/doc/$(PROJ)
	install -m 644 doc/* $(PREFIX)/share/doc/$(PROJ)
	install -m 644 $(ADD_DOCS) $(PREFIX)/share/doc/$(PROJ)

test: 
	./test.sh

stress: stress.c $(LIB)
	$(CC) stress.c $(CFLAGS) $(LIB) -lm `cat config.ldflags` -o $@

win32dist: ahxm.exe
	zip ahxm-$(VERSION).zip ahxm.exe $(ADD_DOCS) doc/*.* examples/*.* samples/*.*
