#!/bin/sh

BIN=$1
[ "$BIN" = "" ] && BIN="./ahxm"

set -e

while read script sum ; do
	echo "Testing ${script}..."
	${BIN} -d wav ${script} > /dev/null
	sum2=$(sha1sum output.wav|cut -d' ' -f1)
	[ "${sum}" = "${sum2}" ]
done <<EOF
examples/alteration.ahs  e65d8c288e1b7b99b47589dd80cdd5c9246d51e5
examples/attack.ahs  3cd69accc680aaf4c553eb6d1fda924530e02397
examples/distort.ahs  52dff3f269b239f4fe5ad4d318ac0bbe9e303a98
examples/eff_fader.ahs  c9cdc1ffd9bf35f89ca483cae557ba96c8df10e4
examples/eff_wobble.ahs  05c08356be0c74a5f0973671db0e0855c68bda78
examples/example2.ahs  b21afb599280cfb2f7d8d711ab1b9d46ae620ce6
examples/example3.ahs  096d8ab033fb963e7d733fe3c786f4fe5b205876
examples/portamento.ahs  b9f83c4c4dc22c3b587365497328077b55e176b9
examples/print_wave_tempo.ahs  a5c209279a0089a01dd46095750657b2d0ed4126
examples/random.ahs  34e924aeb9c201b0d450029a633dafe4edd45ef1
examples/start.ahs  67a7564a56f214d8d300eeefb8b50a6405d9cd48
examples/tempo_change.ahs  e57b6cab266eebe1de49e86d0aebdbbad17bb936
examples/volume.ahs  7ef3ed27f212241eac1e7c690eb66226a9f530d9
EOF

exit 0
