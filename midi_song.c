/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    midi_song.c - MIDI song event stream management

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ahxm.h"


/** data **/

/* the MIDI song stream */

static struct song_ev *midi_song = NULL;
static int n_midi_ev = 0;

/* the MIDI tracks: just a track/channel table */

#define MIDI_TRACK_NUM 256
static int track_channel[MIDI_TRACK_NUM];

/* MIDI message types */

#define MIDI_MSG_NOTE_ON    0x90
#define MIDI_MSG_NOTE_OFF   0x80
#define MIDI_MSG_CONTROLLER 0xB0
#define MIDI_MSG_PROGRAM    0xC0

/* MIDI device fd */
int midi_fd = -1;


/** code **/

static struct song_ev *add_midi_ev(struct song_ev *e)
/* adds a MIDI song event */
{
    if (e->channel < 0)
        return NULL;

    return copy_event(&midi_song, &n_midi_ev, e);
}


static int msecs_type_cmp(const void *v1, const void *v2)
/* MIDI song event compare function for qsort(), by time (for playing) */
{
    struct song_ev *e1;
    struct song_ev *e2;

    e1 = (struct song_ev *) v1;
    e2 = (struct song_ev *) v2;

    if (e1->msecs == e2->msecs)
        return e1->type - e2->type;

    return e1->msecs - e2->msecs;
}


static void midi_song_convert_events(void)
/* converts generic song_ev events to MIDI events */
{
    struct song_ev *e;
    int msecs, msecs_ac, f_msecs;
    double time_ac, time_ac_m;
    int num, den;
    double mspw;
    int n;

    /* resets the MIDI stream */
    if (midi_song != NULL) {
        free(midi_song);
        midi_song = NULL;
    }

    n_midi_ev = 0;

    /* sorts the song */
    song_sort();

    mspw = 0;
    msecs = msecs_ac = f_msecs = 0;
    time_ac = time_ac_m = 0;
    num = den = 4;

    /* by default, all channels are 'mute' until
       a channel is explicitly set */
    for (n = 0; n < MIDI_TRACK_NUM; n++)
        track_channel[n] = -1;

    /* travels the song events generating MIDI song events */
    for (n = 0; n < n_song_ev; n++) {
        /* gets the song event */
        e = &song[n];

        /* calculates the msecs */
        msecs = ((e->time - time_ac) * mspw) + msecs_ac;
        e->msecs = msecs;

        /* if it's not a generic message, set MIDI channel */
        if (e->trk_id >= 0)
            e->channel = track_channel[e->trk_id];

        switch (e->type) {
        case SONG_EV_TEMPO:

            /* updates accumulations */
            msecs_ac = msecs;
            time_ac = e->time;

            /* calculates milliseconds-per-whole based on new tempo */
            mspw = 1000.0 * 60.0;
            mspw /= (e->amount / 4.0);

            break;

        case SONG_EV_METER:

            /* just store the values */
            num = e->min;
            den = e->max;
            time_ac_m = e->time;

            break;

        case SONG_EV_MEASURE:

            song_test_measure_boundary(e->time - time_ac_m,
                                       num, den, e->value);
            break;

        case SONG_EV_MIDI_CHANNEL:

            /* stores the channel for this track */
            track_channel[e->trk_id] = e->channel;
            break;

        case SONG_EV_NOTE:

            /* convert to note on / off pairs */

            e->vel = (int) (e->vol * 127.0);

            add_midi_ev(e);

            e = add_midi_ev(e);
            e->type = SONG_EV_NOTE_OFF;

            msecs += (int) (e->len * mspw);
            e->msecs = msecs;

            break;

        case SONG_EV_BACK:

            /* move the cursor back */
            msecs_ac -= (int) (e->len * mspw);
            break;

        case SONG_EV_MIDI_PROGRAM:

            add_midi_ev(e);
            break;

        case SONG_EV_SS_PITCH_STRETCH:
        case SONG_EV_SS_PRINT_WAVE_TEMPO:
        case SONG_EV_SS_WAV:
        case SONG_EV_SS_PAT:
        case SONG_EV_SS_SF2:
        case SONG_EV_SS_SUSTAIN:
        case SONG_EV_SS_ATTACK:
        case SONG_EV_SS_VIBRATO:
        case SONG_EV_SS_PORTAMENTO:
        case SONG_EV_SS_CHANNEL:
        case SONG_EV_SS_EFF_DELAY:
        case SONG_EV_SS_EFF_ECHO:
        case SONG_EV_SS_EFF_COMB:
        case SONG_EV_SS_EFF_ALLPASS:
        case SONG_EV_SS_EFF_FLANGER:
        case SONG_EV_SS_EFF_WOBBLE:
        case SONG_EV_SS_EFF_SQWOBBLE:
        case SONG_EV_SS_EFF_HFWOBBLE:
        case SONG_EV_SS_EFF_FADER:
        case SONG_EV_SS_EFF_REVERB:
        case SONG_EV_SS_EFF_FOLDBACK:
        case SONG_EV_SS_EFF_ATAN:
        case SONG_EV_SS_EFF_DISTORT:
        case SONG_EV_SS_EFF_OVERDRIVE:
        case SONG_EV_SS_EFF_OFF:
        case SONG_EV_SS_MASTER_VOLUME:
        case SONG_EV_NOP:

            /* ignored */
            break;

        case SONG_EV_SONG_INFO:

            /* song info should be used */
            break;

        case SONG_EV_EOT:

            /* end of track; trigger possible cleaning */
            break;

        case SONG_EV_NOTE_OFF:
        case SONG_EV_END:

            /* never found in generic song streams */
            break;
        }

        /* store the further time seen */
        if (f_msecs < msecs)
            f_msecs = msecs;
    }

    /* generates an end of event mark, a time after the last one */
    e = add_event(&midi_song, &n_midi_ev);

    e->type = SONG_EV_END;
    e->msecs = f_msecs + 1000;
}


int midi_song_play(int skip_secs)
{
#ifdef CONFOPT_NANOSLEEP
    struct song_ev *e;
    int msecs, msecs_p, msecs_d;
    int go;
    struct timespec ts;
    unsigned char midimsg[1024];
    int mi;
    int skip_msecs;

    /* convert the song to MIDI events */
    midi_song_convert_events();

    /* sort by time */
    qsort(midi_song, n_midi_ev, sizeof(struct song_ev), msecs_type_cmp);

    msecs = msecs_p = 0;
    go = 1;
    e = midi_song;

    /* calculate the millisecond to start playing */
    skip_msecs = skip_secs * 1000;

    /* loop the events */
    while (go) {
        /* clear buffer */
        mi = 0;

        if (verbose >= 1 && msecs % 1000 == 0) {
            int m = msecs / 1000;
            printf("[%02d:%02d]\r", m / 60, m % 60);
            fflush(stdout);
        }

        /* process all events for this exact time */
        while (e->msecs == msecs) {
            switch (e->type) {
            case SONG_EV_NOTE:

                midimsg[mi++] = MIDI_MSG_NOTE_ON | e->channel;
                midimsg[mi++] = e->value;
                midimsg[mi++] = e->vel;

                break;

            case SONG_EV_NOTE_OFF:

                midimsg[mi++] = MIDI_MSG_NOTE_OFF | e->channel;
                midimsg[mi++] = e->value;
                midimsg[mi++] = 0;

                break;

            case SONG_EV_MIDI_PROGRAM:

                midimsg[mi++] = MIDI_MSG_PROGRAM | e->channel;
                midimsg[mi++] = e->value;

                break;

            case SONG_EV_END:

                go = 0;
                break;

            default:
                /* ignore the rest */
                break;
            }

            /* next event */
            e++;
        }

        if (!go)
            break;

        /* get time of next event */
        msecs_p = msecs;
        msecs = e->msecs;
        msecs_d = msecs - msecs_p;

        if (msecs >= skip_msecs) {
            /* if there are pending messages, write them */
            if (mi)
                write(midi_fd, midimsg, mi);

            /* calculate the time to sleep */
            ts.tv_sec = (time_t) msecs_d / 1000;
            ts.tv_nsec = (long) ((msecs_d * 1000000) % 1000000000);

            nanosleep(&ts, NULL);
        }
    }

    if (verbose >= 1)
        printf("\n");

#endif                          /* CONFOPT_NANOSLEEP */

    return 0;
}


int midi_device_open(char *devfile)
{
    if (devfile == NULL)
        devfile = "/dev/midi";

    return (midi_fd = open(devfile, O_WRONLY));
}


void midi_device_close(void)
{
    close(midi_fd);
}
