%{
/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    compiler.y - Scripting language YACC parser

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "ahxm.h"


/** data **/

int yylex(void);
void yyerror(const char *s);

/* injecting code functions (defined in compiler.l) */
int push_code(const char *code);
int push_code_from_file(const char *file);

/* current track */
int track;

/* current time */
double cur_time = 0.0;

/* end time (longest time seen) */
double end_time = 0.0;

/* note globals */
static double length;
static int octave;
static int transpose;
static double staccato;
static sample_t volume_from;
static sample_t volume_to;
static double gliss;

/* parser debugging */

#define DEBUGF if(verbose >= 2)printf

/* blocks */

struct block {
    const char *name;
    int n_sblocks;
    char **sblocks;
};

static struct block *blocks = NULL;
static int n_blocks = 0;

/* group info */
struct group {
    double start;
    double gliss;
    double max;
};

static struct group *groups = NULL;
static int n_groups = 0;
static int groups_size = 0;

/* mark info */
struct mark {
    const char *name;
    double time;
};

static struct mark *marks = NULL;
static int n_marks = 0;

/* arpeggiator */
struct arp {
    double delay;
    int transpose;
    sample_t volume;
    int track_off;
};

static struct arp *arps = NULL;
static int n_arps = 0;
static int arps_size = 0;

static double arp_delay;
static int arp_transpose;
static sample_t arp_volume;
static int arp_track_off;

int compiler_error = 0;

extern int yyline;

/* alterations for each note */
int alterations[12];

/* random seeds */
unsigned int block_seed = 0;
unsigned int volume_seed = 0;

/* solo track (-1, no solo) */
int solo_track = -1;


/** code **/

unsigned int ah_rnd(unsigned int *seed)
/* special randomizer */
{
    *seed = (*seed * 58321) + 11113;

    return *seed >> 16;
}


void c_err(const char *e1, const char *e2, const char *e3)
/* reports an error from the compiler */
{
    printf("ahxm:");

    if (e1 != NULL)
        printf(" %s", e1);
    if (e2 != NULL)
        printf(" %s", e2);
    if (e3 != NULL)
        printf(" %s", e3);

    printf(" in line %d\n", yyline);

    compiler_error++;
}


static void forward(double step)
/* moves forward current time */
{
    /* add step */
    cur_time += step;

    /* quantizations could be done here */
}


static sample_t volume(void)
{
    sample_t f;

    if (volume_from == volume_to)
        f = volume_from;
    else {
        unsigned int r = ah_rnd(&volume_seed) % 1000;

        f = volume_from + (volume_to - volume_from) *
            (((sample_t) r) / 1000.0);
    }

    return f;
}


/* blocks */

static int find_block(const char *name)
/* finds a block */
{
    int n;

    for (n = 0; n < n_blocks; n++) {
        if (strcmp(name, blocks[n].name) == 0)
            return n;
    }

    return -1;
}


static int set_block(const char *name, char *block)
/* defines a block */
{
    int n;
    struct block *b;
    char *start;
    char *stop;

    /* if block already exists, free it */
    if ((n = find_block(name)) >= 0) {
        b = &blocks[n];

        /* free all subblocks */
        for (n = 0; n < b->n_sblocks; n++)
            free(b->sblocks[n]);

        /* free the subblock array */
        free(b->sblocks);
    }
    else {
        GROW(blocks, n_blocks, struct block);
        b = &blocks[n_blocks++];

        b->name = strdup(name);
    }

    /* reset */
    b->n_sblocks = 0;
    b->sblocks = NULL;

    /* now split in subblocks (delimited by : ) */
    start = block;

    while ((stop = strchr(start, ':')) != NULL) {
        /* break there */
        *stop = '\0';

        /* add the subblock */
        GROW(b->sblocks, b->n_sblocks, char *);
        b->sblocks[b->n_sblocks++] = strdup(start);

        start = stop + 1;
    }

    /* no more separators? store the rest */
    GROW(b->sblocks, b->n_sblocks, char *);
    b->sblocks[b->n_sblocks++] = strdup(start);

    /* the original block is no longer needed */
    free(block);

    return 1;
}


static void insert_block(const char *name)
/* inserts a block */
{
    int n;

    if ((n = find_block(name)) >= 0) {
        struct block *b;

        b = &blocks[n];

        /* get one of them, randomly */
        n = ah_rnd(&block_seed) % b->n_sblocks;

        push_code(strdup(b->sblocks[n]));
    }
    else
        c_err("block-not-found", name, NULL);
}


static int insert_file(const char *filename)
{
    if (!push_code_from_file(filename)) {
        c_err("script-not-found", filename, NULL);
        return 1;
    }

    return 0;
}


/* groups */

static int push_group(void)
/* starts a new group of notes */
{
    struct group *g;

    if (n_groups == groups_size) {
        GROW(groups, groups_size, struct group);
        groups_size++;
    }

    g = &groups[n_groups++];

    g->start = cur_time;
    g->gliss = 0.0;
    g->max = 0.0;

    return 1;
}


static int next_group_part(void)
/* part delimiter */
{
    struct group *g;

    if (n_groups == 0) {
        c_err("missing-start-of-group", NULL, NULL);
        return 0;
    }

    g = &groups[n_groups - 1];

    /* store maximum frame */
    if (g->max < cur_time)
        g->max = cur_time;

    /* add glissando delay */
    g->gliss += gliss;

    /* rewind */
    cur_time = g->start + g->gliss;

    return 1;
}


static int pop_group(void)
/* finishes a group, moving the frame to the end of the longer part */
{
    if (n_groups == 0) {
        c_err("missing-start-of-group", NULL, NULL);
        return 0;
    }

    n_groups--;

    /* if other parts of group were longer than the last one,
       move pointer there */
    if (groups[n_groups].max > cur_time)
        cur_time = groups[n_groups].max;

    return 1;
}


/* marks */

static int seek_mark(const char *name)
/* seeks a mark by name; returns its offset or -1 */
{
    int n;

    for (n = 0; n < n_marks; n++)
        if (strcmp(marks[n].name, name) == 0)
            return n;

    return -1;
}


static void add_mark(const char *name)
/* creates a new mark */
{
    int n;

    if ((n = seek_mark(name)) == -1) {
        n = n_marks++;
        GROW(marks, n, struct mark);
        marks[n].name = strdup(name);
    }

    marks[n].time = cur_time;
}


static void find_mark(const char *name, int set)
/* finds a mark by name, optionally moving time cursor there */
{
    int n;

    if ((n = seek_mark(name)) != -1) {
        if (set) {
            if (cur_time > marks[n].time) {
                /* if mark is not END, fail */
                if (strcmp(name, "END") != 0)
                    c_err("mark-overpassed", name, NULL);
            }
            else
                cur_time = marks[n].time;
        }
        else
        if (cur_time != marks[n].time)
            c_err("mark-mismatch", name, NULL);

        return;
    }

    c_err("mark-not-found", name, NULL);
}


/* arpeggiator */

static void arp_default(void)
/* resets arpeggiator values to the default ones */
{
    arp_delay = 0.0;
    arp_transpose = 0;
    arp_volume = 1.0;
    arp_track_off = 0;
}


static void add_arp(void)
/* adds an arpeggiator note */
{
    struct arp *a;

    /* if the note is exactly the same, do nothing */
    if (arp_delay == 0.0 && arp_transpose == 0 &&
        arp_volume == 1.0 && arp_track_off == 0)
        return;

    if (n_arps == arps_size) {
        GROW(arps, arps_size, struct arp);
        arps_size++;
    }

    a = &arps[n_arps];

    a->delay = arp_delay;
    a->transpose = arp_transpose;
    a->volume = arp_volume;
    a->track_off = arp_track_off;

    n_arps++;
    arp_default();
}


static void set_alteration(const char *altstr)
/* sets the alterations from altstr */
{
    int n, steps[] = { 2, 0, 2, 0, 1, 2, 0, 2, 0, 2, 0, 1 };

    /* reset alterations */
    for (n = 0; n < 12; n++)
        alterations[n] = 0;

    /* changed according the altstr spec */
    for (n = 0; *altstr != '\0' && n < 12; altstr++) {
        switch (*altstr) {
        case '&':
            alterations[n] = -1;
            break;

        case '#':
            alterations[n] = 1;
            break;
        }

        /* move to next natural note */
        n += steps[n];
    }
}


/* song events */

static struct song_ev *add_song_ev(song_ev_type type)
{
    struct song_ev *r;

    r = add_event(&song, &n_song_ev);

    r->type = type;
    r->time = cur_time;
    r->trk_id = track;
    r->event_id = n_song_ev - 1;

    return r;
}


static void add_note_event(int note)
/* adds a note event */
{
    int n;
    int np;
    struct song_ev *e;

    /* sum the alteration */
    if ((n = note % 12) < 0)
        n += 12;

    note += alterations[n];

    /* calculate the note */
    np = note + transpose + (octave * 12);

    /* is note out of range? */
    if (np < 0 || np > 127) {
        c_err("note-out-of-range", NULL, NULL);
        return;
    }

    e = add_song_ev(SONG_EV_NOTE);

    e->value = np;
    e->len = length * staccato;
    e->vol = volume();

    /* add arpeggiator repetitions */
    for (n = 0; n < n_arps; n++) {

        e = add_song_ev(SONG_EV_NOTE);

        e->time = cur_time + arps[n].delay;
        e->trk_id = track + arps[n].track_off;
        e->value = np + arps[n].transpose;
        e->len = length * staccato;
        e->vol = volume() * arps[n].volume;
    }
}


static void add_back_event(void)
{
    struct song_ev *e = add_song_ev(SONG_EV_BACK);

    e->len = length;
}


static void add_tempo_event(int trk_id, double tempo)
{
    struct song_ev *e = add_song_ev(SONG_EV_TEMPO);

    e->trk_id = trk_id;
    e->amount = tempo;
}


static void add_meter_event(int trk_id, int num, int den)
{
    struct song_ev *e = add_song_ev(SONG_EV_METER);

    e->trk_id = trk_id;
    e->min = num;
    e->max = den;
}

static void add_measure_event(void)
{
    struct song_ev *e = add_song_ev(SONG_EV_MEASURE);

    e->trk_id = -1;
    e->value = yyline;
}


static void add_ss_sustain_event(double sustain)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_SUSTAIN);

    e->amount = sustain;
}


static void add_ss_attack_event(double attack)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_ATTACK);

    e->amount = attack;
}


static void add_ss_vibrato_event(double depth, double freq)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_VIBRATO);

    e->depth = depth;
    e->freq = freq;
}


static void add_ss_portamento_event(double portamento)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_PORTAMENTO);

    e->amount = portamento;
}


static void add_ss_channel_event(int channel, sample_t vol)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_CHANNEL);

    e->channel = channel;
    e->vol = vol;
}


static void add_ss_wav_event(const char *wav_file, int base, int min,
                             int max, double loop_start, double loop_end,
                             int first_channel, int skip_channels)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_WAV);

    e->name = wav_file;
    e->value = base;
    e->min = min;
    e->max = max;
    e->start = loop_start;
    e->end = loop_end;
    e->channel = first_channel;
    e->skip_channels = skip_channels;
}


static void add_ss_pat_event(const char *pat_file)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_PAT);

    e->name = pat_file;
}


static void add_ss_sf2_event(const char *sf2_file, const char *insname)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_SF2);

    e->name = sf2_file;
    e->str2 = insname;
}


static void add_ss_eff_event(int type, int channel, double size,
                             sample_t gain, double depth, double freq,
                             double phase, sample_t initial,
                             sample_t final)
{
    struct song_ev *e = add_song_ev(type);

    e->channel = channel;
    e->len = size;
    e->vol = gain;
    e->depth = depth;
    e->freq = freq;
    e->phase = phase;
    e->initial = initial;
    e->final = final;
}


static void add_ss_pitch_stretch(int note, double len, sample_t vol)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_PITCH_STRETCH);

    e->value = note;
    e->len = len;
    e->vol = vol;
}


static void add_ss_print_wave_tempo(int note, double len)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_PRINT_WAVE_TEMPO);

    e->value = note;
    e->len = len;
}


static void add_ss_master_volume(sample_t vol)
{
    struct song_ev *e = add_song_ev(SONG_EV_SS_MASTER_VOLUME);

    e->trk_id = -1;
    e->vol = vol;
}


static void add_midi_channel_event(int channel)
{
    struct song_ev *e = add_song_ev(SONG_EV_MIDI_CHANNEL);

    e->channel = channel - 1;
}


static void add_midi_program_event(int program)
{
    struct song_ev *e = add_song_ev(SONG_EV_MIDI_PROGRAM);

    e->value = program;
}


static void add_song_info_event(const char *author, const char *name)
{
    struct song_ev *e = add_song_ev(SONG_EV_SONG_INFO);

    e->str2 = author;
    e->name = name;
}


static void init_track(void)
/* sets the default values for a new track */
{
    int n;

    /* is there an end time? test if this is longer */
    if (cur_time > end_time) {
        add_mark("END");
        end_time = cur_time;
    }

    cur_time = 0.0;
    length = 0.0;
    transpose = 0;
    staccato = 0.8;
    volume_from = volume_to = 0.75;
    octave = 5;
    gliss = 0;

    /* groups should not cross track boundaries */
    n_groups = 0;

    /* reset arpeggiator */
    n_arps = 0;
    arp_default();

    /* is there a START mark? if so, move there */
    if ((n = seek_mark("START")) != -1)
        cur_time = marks[n].time;

    /* reset alterations */
    for (n = 0; n < 12; n++)
        alterations[n] = 0;
}

%}

%union {
	int i;
	double d;
	char * p;
};

%token <i> P_INTEGER S_INTEGER
%token <d> P_REAL S_REAL
%token <i> NOTE_P NOTE_T3 NOTE_T5
%token <p> NEW_MARK GOTO_MARK ASSERT_MARK

%token <p> BLOCK BLK_ASSIGN BLK_INSERT FILE_INSERT

%token <p> ALTSTR

%token <p> LIBPATH_ADD

%token <p> XC_STR
%token <i> XC_ABSNOTE
%token <d> XC_MSECS

%token SS_SEP SS_WAV SS_LOOP_WAV SS_PAT SS_SF2
%token SS_SUSTAIN SS_ATTACK SS_VIBRATO SS_PORTAMENTO SS_CHANNEL SS_VOL SS_MASTER_VOL

%token SS_EFF_DELAY SS_EFF_ECHO SS_EFF_COMB SS_EFF_ALLPASS SS_EFF_FLANGER
%token SS_EFF_WOBBLE SS_EFF_SQWOBBLE SS_EFF_HFWOBBLE
%token SS_EFF_FADER SS_EFF_REVERB 
%token SS_EFF_FOLDBACK SS_EFF_ATAN SS_EFF_DISTORT SS_EFF_OVERDRIVE
%token SS_EFF_OFF

%token SS_PITCH_STRETCH SS_TIME_STRETCH SS_PRINT_WAVE_TEMPO

%token SONG_INFO

%token MIDI_CHANNEL MIDI_PROGRAM MIDI_GENERIC

%type <i> integer note note_pitch rest back
%type <d> real p_number number note_length

%type <d> arp_list arp_note
%type <i> xc_absnote

%%

script:
	script stmt		{ ; }
	| /* NULL */
	;

stmt:
	note			{
					/* note event */
					add_note_event($1);
					forward(length);
				}
	| rest			{
					/* rest */
					forward(length);
				}
	| back			{
					/* back */
					add_back_event();
				}
	| 'z' note_length	{
					/* zero note */
					length = $2;
				}
	| 'o' P_INTEGER		{
					/* absolute octave */
					octave = $2;
				}
	| 'o' S_INTEGER		{
					/* relative octave */
					octave += $2;
				}
	| 'v' P_INTEGER		{
					/* absolute volume */
					volume_from = volume_to = $2;
				}
	| 'v' P_REAL		{
					/* absolute volume */
					volume_from = volume_to = $2;
				}
	| 'v' S_REAL		{
					/* relative volume */
					volume_from += $2;
					volume_to += $2;
				}
	| 'v' P_REAL ',' P_REAL	{
					/* absolute volume ranges */
					volume_from = $2;
					volume_to = $4;
				}
	| 't' integer		{
					/* transpose */
					transpose = $2;
				}
	| 's' p_number		{
					/* staccato */
					staccato = $2;
				}

	| '<'			{
					/* start of group */
					push_group();
				}
	| ';'			{
					/* group delimiter */
					next_group_part();
				}
	| '>'			{
					/* end of group */
					pop_group();
				}
	| 'l' note_length	{
					/* glissando */
					gliss = $2;
				}

	| '|'			{
					/* measure mark event */
					add_measure_event();
				}

	| NEW_MARK		{
					/* add new mark */
					add_mark($1);
				}
	| GOTO_MARK		{
					/* go to mark */
					find_mark($1, 1);
				}
	| ASSERT_MARK		{
					/* assert mark */
					find_mark($1, 0);
				}

	| '\\'			{
					/* new track */

					track++;
					init_track();
				}
	| 'T' p_number		{
					/* tempo setting */
					add_tempo_event(-1, $2);
				}
	| 'M' P_INTEGER '/' P_INTEGER {
					/* meter (time signature) setting */
					add_meter_event(-1, $2, $4);
				}
	| ALTSTR		{
					/* alteration string */
					set_alteration($1);
				}

	| BLOCK '*' P_INTEGER	{
					/* repeat block */
					int n;

					/* store the block as <TMP> */
					set_block("<TMP>", $1);

					for(n = 0;n < $3;n++)
						insert_block("<TMP>");
				}
	| BLOCK BLK_ASSIGN	{
					/* assign block */
					set_block($2, $1);
				}
	| BLK_INSERT		{
					/* insert block */
					insert_block($1);
				}
	| FILE_INSERT		{
					/* insert file */
					insert_file($1);
				}
	| LIBPATH_ADD		{
					/* add library path */
					libpath_add($1, 0);
				}

	| arp			{ ; }

	| xc_cmd		{ ; }

	;

integer:
	P_INTEGER		{ $$ = $1; }
	| S_INTEGER		{ $$ = $1; }
	;

real:
	P_REAL			{ $$ = $1; }
	| S_REAL		{ $$ = $1; }
	;

p_number:
	P_INTEGER		{ $$ = (double) $1; }
	| P_REAL		{ $$ = $1; }
	;

number:
	integer			{ $$ = (double) $1; }
	| real			{ $$ = $1; }
	;

note:
	note_pitch		{ $$ = $1; }
	| note note_length	{ $$ = $1; length=$2; }
	| note '~' number	{ $$ = $1; DEBUGF("imm volume: %lf\n", $3); }
	;

note_pitch:
	NOTE_P			{ $$ = $1; }
	| note_pitch '&'	{ $$ = $1 - 1; }
	| note_pitch '#'	{ $$ = $1 + 1; }
	| note_pitch '\''	{ $$ = $1 + 12; }
	| note_pitch ','	{ $$ = $1 - 12; }
	;

note_length:
	P_INTEGER		{ $$ = 1 / (double) $1; }
	| note_length NOTE_T3	{ $$ = $1 * 2.0 / 3.0; }
	| note_length NOTE_T5	{ $$ = $1 * 4.0 / 5.0; }
	| note_length '*' p_number { $$ = $1 * $3; }
	| note_length '.'	{ $$ = $1 * 1.5; }
	;

rest:
	'r'			{ ; }
	| rest note_length	{
					/* rest with length */
					length = $2;
				}
	;

back:
	'k'			{ ; }
	| back note_length	{
					/* back with length */
					length = $2;
				}
	;

arp:
	'x'			{
					/* empty arpeggiator */
					n_arps = 0;
					arp_default();
				}
	| 'x' arp_list		{ ; }
	;

arp_list:
	arp_note		{
					/* first arpeggiator note */
					n_arps = 0;
					add_arp();
				}
	| arp_list ',' arp_note	{
					/* rest of arpeggiator notes */
					add_arp();
				}
	;

arp_note:
	note_length		{
					/* arpeggiator delay */
					arp_delay = $1;
				}
	| arp_note '~' number	{
					/* arpeggiator volume */
					arp_volume = $3;
				}
	| arp_note S_INTEGER	{
					/* arpeggiator transpose */
					arp_transpose = $2;
				}
	| arp_note '/' P_INTEGER {
					/* arpeggiator track offset */
					arp_track_off = $3;
				}
	| arp_note NOTE_T3	{
					/* HACK: /3 */
					arp_track_off = 3;
				}
	| arp_note NOTE_T5	{
					/* HACK: /5 */
					arp_track_off = 5;
				}
	;

xc_absnote:
	P_INTEGER		{ $$ = $1; }
	| XC_ABSNOTE		{ $$ = $1; }
	;

xc_cmd:
	SS_WAV XC_STR xc_absnote {
					/* load .wav file with just one note */
					add_ss_wav_event($2, $3, $3, $3, -1.0, -1.0, 0, 0);
				}
	| SS_WAV XC_STR xc_absnote xc_absnote xc_absnote {
					/* load .wav file */
					add_ss_wav_event($2, $3, $4, $5, -1.0, -1.0, 0, 0);
				}
	| SS_WAV XC_STR xc_absnote xc_absnote xc_absnote number number {
					/* load .wav file, with loop boundaries */
					add_ss_wav_event($2, $3, $4, $5, $6, $7, 0, 0);
				}
	| SS_WAV XC_STR xc_absnote xc_absnote xc_absnote number number number number {
					/* load .wav file, with loop boundaries,
					   first channel and skip channels */
					add_ss_wav_event($2, $3, $4, $5, $6, $7, $8, $9);
				}
	| SS_PAT XC_STR		{
					/* load .pat file */
					add_ss_pat_event($2);
				}
	| SS_SF2 XC_STR		{
					/* list instruments in .sf2 file */
					add_ss_sf2_event($2, NULL);
				}
	| SS_SF2 XC_STR XC_STR	{
					/* load instrument from .sf2 file */
					add_ss_sf2_event($2, $3);
				}

	| SS_SUSTAIN XC_MSECS	{
					/* sets sustain */
					add_ss_sustain_event($2);
				}
	| SS_ATTACK XC_MSECS	{
					/* sets attack */
					add_ss_attack_event($2);
				}
	| SS_VIBRATO XC_MSECS number {
					/* sets vibrato */
					add_ss_vibrato_event($2, $3);
				}
	| SS_PORTAMENTO number {
					/* sets portamento */
					add_ss_portamento_event($2);
				}
	| SS_CHANNEL integer number {
					/* sets volume for a channel */
					add_ss_channel_event($2, $3);
				}
	| SS_VOL number number {
					/* set vol for 2 channels */
					add_ss_channel_event(0, $2);
					add_ss_channel_event(1, $3);
				}
	| SS_VOL number number number {
					/* set vol for 3 channels */
					add_ss_channel_event(0, $2);
					add_ss_channel_event(1, $3);
					add_ss_channel_event(2, $4);
				}
	| SS_VOL number number number number {
					/* set vol for 4 channels */
					add_ss_channel_event(0, $2);
					add_ss_channel_event(1, $3);
					add_ss_channel_event(2, $4);
					add_ss_channel_event(3, $5);
				}
	| SS_VOL number number number number number number {
					/* set vol for 6 channels */
					add_ss_channel_event(0, $2);
					add_ss_channel_event(1, $3);
					add_ss_channel_event(2, $4);
					add_ss_channel_event(3, $5);
					add_ss_channel_event(4, $6);
					add_ss_channel_event(5, $7);
				}
	| SS_EFF_DELAY integer XC_MSECS {
					/* delay effect */
					add_ss_eff_event(SONG_EV_SS_EFF_DELAY,
					$2, $3, 0, 0, 0, 0, 0, 0);
				}

	| SS_EFF_ECHO integer XC_MSECS number {
					/* echo effect */
					add_ss_eff_event(SONG_EV_SS_EFF_ECHO,
					$2, $3, $4, 0, 0, 0, 0, 0);
				}

	| SS_EFF_COMB integer XC_MSECS number {
					/* comb effect */
					add_ss_eff_event(SONG_EV_SS_EFF_COMB,
					$2, $3, $4, 0, 0, 0, 0, 0);
				}

	| SS_EFF_ALLPASS integer XC_MSECS number {
					/* allpass effect */
					add_ss_eff_event(SONG_EV_SS_EFF_ALLPASS,
					$2, $3, $4, 0, 0, 0, 0, 0);
				}

	| SS_EFF_FLANGER integer XC_MSECS number XC_MSECS number number {
					/* flanger effect */
					add_ss_eff_event(SONG_EV_SS_EFF_FLANGER,
					$2, $3, $4, $5, $6, $7, 0, 0);
				}

	| SS_EFF_WOBBLE integer number number {
					/* wobble effect */
					add_ss_eff_event(SONG_EV_SS_EFF_WOBBLE,
					$2, 0, 0.8, 0, $3, $4, 0, 0);
				}

	| SS_EFF_WOBBLE integer number number number {
					/* wobble effect, with gain */
					add_ss_eff_event(SONG_EV_SS_EFF_WOBBLE,
					$2, 0, $5, 0, $3, $4, 0, 0);
				}

	| SS_EFF_SQWOBBLE integer number number {
					/* square wobble effect */
					add_ss_eff_event(SONG_EV_SS_EFF_SQWOBBLE,
					$2, 0, 0, 0, $3, $4, 0, 0);
				}

	| SS_EFF_HFWOBBLE integer number number {
					/* half wobble effect */
					add_ss_eff_event(SONG_EV_SS_EFF_HFWOBBLE,
					$2, 0, 0, 0, $3, $4, 0, 0);
				}

	| SS_EFF_FADER integer XC_MSECS number number {
					/* fader effect */
					add_ss_eff_event(SONG_EV_SS_EFF_FADER,
					$2, $3, 0, 0, 0, 0, $4, $5);
				}

	| SS_EFF_REVERB integer {
					/* reverb effect */
					add_ss_eff_event(SONG_EV_SS_EFF_REVERB,
					$2, 0, 0, 0, 0, 0, 0, 0);
				}

	| SS_EFF_FOLDBACK integer number {
					/* foldback distortion effect */
					add_ss_eff_event(SONG_EV_SS_EFF_FOLDBACK,
					$2, 0, $3, 0, 0, 0, 0, 0);
				}
	| SS_EFF_ATAN integer number {
					/* atan distortion effect */
					add_ss_eff_event(SONG_EV_SS_EFF_ATAN,
					$2, 0, $3, 0, 0, 0, 0, 0);
				}
	| SS_EFF_DISTORT integer number {
					/* distort distortion effect */
					add_ss_eff_event(SONG_EV_SS_EFF_DISTORT,
					$2, 0, $3, 0, 0, 0, 0, 0);
				}
	| SS_EFF_OVERDRIVE integer number {
					/* overdrive distortion effect */
					add_ss_eff_event(SONG_EV_SS_EFF_OVERDRIVE,
					$2, 0, $3, 0, 0, 0, 0, 0);
				}

	| SS_EFF_OFF integer {
					/* off effect */
					add_ss_eff_event(SONG_EV_SS_EFF_OFF,
					$2, 0, 0, 0, 0, 0, 0, 0);
				}

	| SS_PITCH_STRETCH xc_absnote number number {
					/* pitch stretch note */
					add_ss_pitch_stretch($2, $3, $4);

					forward($3);
				}

	| SS_PRINT_WAVE_TEMPO xc_absnote number {
					/* print tempo from wave */
					add_ss_print_wave_tempo($2, $3);
				}

	| SS_MASTER_VOL number	{
					/* set master volume */
					add_ss_master_volume($2);
				}

	| SONG_INFO XC_STR XC_STR {
					/* add song info */
					add_song_info_event($2, $3);
				}

	| MIDI_CHANNEL integer {
					/* midi channel */
					add_midi_channel_event($2);
				}

	| MIDI_PROGRAM integer {
					/* midi program */
					add_midi_program_event($2);
				}
	;

%%

void yyerror(const char *s)
{
    c_err(s, NULL, NULL);
}


#define set_block_d(n,b) set_block(n,strdup(b))

static void compile_startup(void)
{
    track = 0;
    yyline = 1;
    compiler_error = 0;

    /* default settings */
    add_tempo_event(-2, 120.0);
    add_meter_event(-2, 4, 4);
    init_track();

    /* standard tonalities */
    set_block_d("CM", "A");
    set_block_d("Am", "$CM");
    set_block_d("C#M", "A#######");
    set_block_d("A#m", "$C#M");
    set_block_d("DM", "A#--#---");
    set_block_d("Bm", "$DM");
    set_block_d("E&M", "A--&--&&");
    set_block_d("Cm", "$E&M");
    set_block_d("EM", "A##-##--");
    set_block_d("C#m", "$EM");
    set_block_d("FM", "A------&");
    set_block_d("Dm", "$FM");
    set_block_d("F#M", "A######-");
    set_block_d("D#m", "$F#M");
    set_block_d("GM", "A---#---");
    set_block_d("Em", "$GM");
    set_block_d("A&M", "A-&&--&&");
    set_block_d("Fm", "$A&M");
    set_block_d("AM", "A#--##--");
    set_block_d("F#m", "$AM");
    set_block_d("B&M", "A--&---&");
    set_block_d("Gm", "$B&M");
    set_block_d("BM", "A##-###-");
    set_block_d("G#m", "$BM");
}


static int do_parse(void)
{
    int r = yyparse() + compiler_error;

    if (solo_track != -1)
        mute_tracks(-solo_track);

    return r;
}


int compile_ahs_string(const char *code)
{
    compile_startup();

    push_code(strdup(code));

    return do_parse();
}


int compile_ahs(const char *file)
{
    compile_startup();

    if (insert_file(file))
        return 1;

    return do_parse();
}
