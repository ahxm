/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

/* HARD LIMITS */

#ifndef SS_MAX_CHANNELS
#define SS_MAX_CHANNELS 16
#endif

#ifndef SS_MAX_GENERATORS
#define SS_MAX_GENERATORS 256
#endif

#ifndef SS_MAX_INSTRUMENTS
#define SS_MAX_INSTRUMENTS 256
#endif

/* STRUCTURES AND TYPES */

/* samples */

typedef double sample_t;

/* waves */

struct ss_wave {
    double size;                /* size in frames of full wave */
    int p_size;                 /* size in frames of the page */
    int n_channels;             /* # of channels */
    sample_t **wave;            /* the PCM waves */
    int s_rate;                 /* original sample rate */

    double loop_start;          /* start of loop (-1, no loop) */
    double loop_end;            /* end of loop */

    double base_freq;           /* base frequency */
    double min_freq;            /* minimum frequency */
    double max_freq;            /* maximum frequency */

    int first_channel;          /* first channel to spread into */
    int skip_channels;          /* channels to skip when spreading */

    const char *filename;       /* filename (for paged files) */
    long f_pos;                 /* file position where the PCM files */
    int p_offset;               /* offset in frames of the page */
    int bits;                   /* bits of the PCM wave (8, 16) */
    int sign;                   /* sign of the PCM wave (1, -1) */

    int page_faults;            /* number of times ss_load_page() was called */
};

/* generators */

struct ss_gen {
    int note_id;                /* note ID */
    sample_t vol;               /* volume */
    struct ss_wave *w;          /* the wave data */

    double cursor;              /* offset to next sample */
    double inc;                 /* increment (frequency) */

    int sustain;                /* number of frames to play after release */
    sample_t dsvol;             /* volume decrement in sustain */

    int attack;                 /* number of frames of fade-in attack */
    sample_t davol;             /* volume increment in attack */

    double vibrato;             /* vibrato oscillator */
    double vib_depth;           /* vibrato depth */
    double vib_inc;             /* increment for vibrato oscillator */

    double portamento;          /* portamento sum to inc */

    struct ss_gen *next;
    struct ss_gen *prev;
};

/* software syntesizer instruments */

struct ss_ins {
    int n_layers;               /* # of layers */
    struct ss_wave **layers;    /* layers */

    struct ss_gen *gens;        /* generator queue */

    int n_channels;             /* number of channels */
    sample_t *vols;             /* volumes (1 per channel) */
    struct ss_eff **effs;       /* effect chains (1 per channel) */

    double sustain;             /* sustain in msecs */
    double attack;              /* attack in msecs */
    double vib_depth;           /* vibrato depth */
    double vib_freq;            /* vibrato frequency */

    int disabled;               /* if set, no processing is done */

    double portamento;          /* portamento */
};

/* digital effects */

struct ss_eff {
    struct ss_wave *wave;       /* wave buffer */
    sample_t gain;              /* effect gain */
    sample_t igain;             /* gain increment */

    double lfo;                 /* lfo value */
    double lfo_depth;           /* lfo depth */
    double lfo_inc;             /* lfo increment */

    double cursor;              /* current buffer position */

    sample_t(*func) (struct ss_eff *, sample_t);       /* processing function */

    struct ss_eff *next;        /* next in chain */
};

/* song events */

typedef enum {
    SONG_EV_TEMPO,
    SONG_EV_METER,
    SONG_EV_MEASURE,

    SONG_EV_SS_SUSTAIN,
    SONG_EV_SS_ATTACK,
    SONG_EV_SS_VIBRATO,
    SONG_EV_SS_PORTAMENTO,
    SONG_EV_SS_CHANNEL,

    SONG_EV_SS_WAV,
    SONG_EV_SS_PAT,
    SONG_EV_SS_SF2,

    SONG_EV_SS_EFF_OFF,
    SONG_EV_SS_EFF_DELAY,
    SONG_EV_SS_EFF_ECHO,
    SONG_EV_SS_EFF_COMB,
    SONG_EV_SS_EFF_ALLPASS,
    SONG_EV_SS_EFF_FLANGER,
    SONG_EV_SS_EFF_WOBBLE,
    SONG_EV_SS_EFF_SQWOBBLE,
    SONG_EV_SS_EFF_HFWOBBLE,
    SONG_EV_SS_EFF_FADER,
    SONG_EV_SS_EFF_REVERB,

    SONG_EV_SS_EFF_FOLDBACK,
    SONG_EV_SS_EFF_ATAN,
    SONG_EV_SS_EFF_DISTORT,
    SONG_EV_SS_EFF_OVERDRIVE,

    SONG_EV_MIDI_CHANNEL,
    SONG_EV_MIDI_PROGRAM,

    SONG_EV_BACK,
    SONG_EV_NOTE_OFF,
    SONG_EV_NOTE,
    SONG_EV_SS_PITCH_STRETCH,
    SONG_EV_SS_PRINT_WAVE_TEMPO,
    SONG_EV_SS_MASTER_VOLUME,

    SONG_EV_SONG_INFO,

    SONG_EV_EOT,
    SONG_EV_END,

    SONG_EV_NOP
} song_ev_type;


struct song_ev {
    song_ev_type type;          /* event type */
    double time;                /* event time (1: whole note) */
    int frame;                  /* frame number (ss) */
    int msecs;                  /* milliseconds (MIDI) */
    int trk_id;                 /* track id */
    int event_id;               /* event id */
    int note_id;                /* note id */
    int value;                  /* MIDI-like note (or base) */
    int min;                    /* MIDI-like minimum note */
    int max;                    /* MIDI-like maximum note */
    int channel;                /* channel (or first channel) */
    int skip_channels;          /* channels to skip when spreading */
    int vel;                    /* MIDI velocity */
    sample_t vol;               /* volume or gain */
    sample_t initial;           /* initial vol */
    sample_t final;             /* final vol */
    double amount;              /* sust, attck or portmnt (frames), tempo in bpm */
    double len;                 /* note length (1: whole note) or effect size */
    double depth;               /* depth */
    double freq;                /* freq */
    double start;               /* loop start */
    double end;                 /* loop end */
    double phase;               /* phase */
    const char *name;           /* file name, or track name */
    const char *str2;           /* track author, or instrument in sf2 */
};

/* GLOBALS */

/* in ss_core.c */

extern int ss_frequency;
extern int ss_interpolation;
extern int ss_nchannels;

/* in ss_input.c */

extern int ss_page_size;

/* in ss_output.c */

extern sample_t ss_master_volume;
extern int ss_output_clipped;
extern sample_t ss_optimal_volume;
extern const char *ss_cue_file_name;

/* in song.c */

extern struct song_ev *song;
extern int n_song_ev;
extern int n_song_tracks;

/* in compiler.y */

extern int solo_track;

/* in support.c */

extern int verbose;
extern int trace;

/* MACROS */

/* milliseconds to frames conversion */
#define MS2F(ms) ((ms / 1000.0) * ss_frequency)

/* grows a dynamic array */
#define GROW(b,n,t) b = (t *)realloc(b,((n) + 1) * sizeof(t))

/* PROTOTYPES */

/* in ss_core.c */

double ss_note_frequency(int note);
struct ss_wave *ss_alloc_wave(int size, int n_channels, int s_rate,
                              int p_size);
void ss_free_wave(struct ss_wave *w);
void ss_prepare_wave(struct ss_wave *w);
sample_t ss_get_sample(struct ss_wave *w, int channel, double offset);
double ss_tempo_from_wave(const struct ss_wave *w, int note, double len);
double ss_pitch_from_tempo(const struct ss_wave *w, double tempo,
                           double len);

/* in ss_gen.c */

void ss_gen_init(void);
struct ss_gen *ss_gen_alloc(struct ss_gen **q);
void ss_gen_free(struct ss_gen **q, struct ss_gen *g);
void ss_gen_sustain(struct ss_gen *g, double sustain);
void ss_gen_attack(struct ss_gen *g, double attack);
void ss_gen_vibrato(struct ss_gen *g, double depth, double freq);
void ss_gen_portamento(struct ss_gen *g, double portamento);
void ss_gen_play(struct ss_gen *g, double freq, sample_t vol, int note_id,
                 struct ss_wave *w);
void ss_gen_release(struct ss_gen *g);
int ss_gen_frame(struct ss_gen *g, int n_channels, sample_t frame[]);

/* in ss_ins.c */

void ss_ins_init(struct ss_ins *i);
void ss_ins_disable(struct ss_ins *i);
void ss_ins_add_layer(struct ss_ins *i, struct ss_wave *w);
struct ss_wave *ss_ins_find_layer(const struct ss_ins *i, double freq,
                                  int *off);
void ss_ins_set_channel(struct ss_ins *i, int channel, sample_t vol);
void ss_ins_set_sustain(struct ss_ins *i, double sustain);
void ss_ins_set_attack(struct ss_ins *i, double attack);
void ss_ins_set_vibrato(struct ss_ins *i, double depth, double freq);
void ss_ins_set_portamento(struct ss_ins *i, double portamento);
int ss_ins_play(struct ss_ins *i, double freq, sample_t vol, int note_id,
                struct ss_wave *w);
int ss_ins_note_on(struct ss_ins *i, int note, sample_t vol, int note_id);
void ss_ins_note_off(struct ss_ins *i, int note_id);
int ss_ins_frame(struct ss_ins *i, sample_t frame[]);

/* in ss_output.c */

int ss_output_open(const char *drvname, const char *filename);
void ss_output_init_frame(sample_t frame[]);
int ss_output_write(sample_t frame[]);
void ss_output_close(void);
int cue_file_song_info(int frame, const char *author, const char *name);

/* in ss_input.c */

void load_pcm_wave(FILE * f, struct ss_wave *w);
struct ss_wave *ss_load_wav_file(const char *file,
                                 double base_freq, double min_freq,
                                 double max_freq, double loop_start,
                                 double loop_end, int first_channel,
                                 int skip_channels);
int ss_load_pat_file(struct ss_ins *i, const char *file);
int ss_load_sf2_file(struct ss_ins *i, const char *file,
                     const char *iname);

/* in ss_eff.c */

void ss_eff_delay(struct ss_eff **ec, double size);
void ss_eff_echo(struct ss_eff **ec, double size, sample_t gain);
void ss_eff_comb(struct ss_eff **ec, double size, sample_t gain);
void ss_eff_allpass(struct ss_eff **ec, double size, sample_t gain);
void ss_eff_flanger(struct ss_eff **ec, double size, sample_t gain,
                    double depth, double freq, double phase);
void ss_eff_wobble(struct ss_eff **ec, double freq, double phase,
                   sample_t gain);
void ss_eff_square_wobble(struct ss_eff **ec, double freq, double phase);
void ss_eff_half_wobble(struct ss_eff **ec, double freq, double phase);
void ss_eff_fader(struct ss_eff **ec, double size, sample_t initial,
                  sample_t final);
void ss_eff_reverb(struct ss_eff **ec);
void ss_eff_foldback(struct ss_eff **ec, sample_t threshold);
void ss_eff_atan(struct ss_eff **ec, sample_t gain);
void ss_eff_distort(struct ss_eff **ec, sample_t gain);
void ss_eff_overdrive(struct ss_eff **ec, sample_t gain);

sample_t ss_eff_process(struct ss_eff *e, sample_t s);
void ss_eff_off(struct ss_eff **ec);

/* in song.c */

void song_clear(void);
struct song_ev *add_event(struct song_ev **song, int *count);
struct song_ev *copy_event(struct song_ev **song, int *count,
                           const struct song_ev *e);
void dump_song_event(const struct song_ev *e);
void dump_song_events(const struct song_ev *song, int n_song_ev);
void mute_tracks(int trk_id);
void song_sort(void);
int song_test_measure_boundary(double ev_time, int num, int den, int line);

/* in ss_song.c */

int ss_song_render(int skip_secs, const char *driver, const char *devfile);

/* in midi_song.c */

int midi_song_play(int skip_secs);
int midi_device_open(char *devfile);
void midi_device_close(void);

/* in compiler.y */

int compile_ahs_string(const char *code);
int compile_ahs(const char *file);

/* in support.c */

void libpath_add(const char *path, int strip);
FILE *libpath_fopen(const char *filename, const char *mode);
char *libpath_locate(const char *filename);
void libpath_print(void);
void transconv_add(const char *from, const char *to, const char *convcmd);
char *transconv_pipe(const char *cmd, const char *ext, const char *dir);
char *transconv(const char *file, const char *ext, const char *dir);
