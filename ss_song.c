/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    ss_song.c - Software synth song event stream management

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ahxm.h"


/** data **/

/* the softsynth song stream */

static struct song_ev *ss_song = NULL;
static int n_ss_ev = 0;

/* the instruments */

struct ss_ins ss_song_ins[SS_MAX_INSTRUMENTS];


/** code **/

static struct song_ev *add_ss_ev(const struct song_ev *e)
/* adds a softsynth song event */
{
    return copy_event(&ss_song, &n_ss_ev, e);
}


static int frame_type_eventid_cmp(const void *v1, const void *v2)
/* softsynth song event compare function for qsort() */
{
    struct song_ev *e1;
    struct song_ev *e2;
    int ret;

    e1 = (struct song_ev *) v1;
    e2 = (struct song_ev *) v2;

    ret = e1->frame - e2->frame;

    if (ret == 0)
        ret = e1->type - e2->type;

    if (ret == 0)
        ret = e1->event_id - e2->event_id;

    return ret;
}


static void ss_song_convert_events(int *n_channels)
/* converts generic song_ev events to softsynth events */
{
    int note_id = 1;
    struct song_ev *e;
    int frame, frame_ac, f_frame;
    double fpw, time_ac, time_ac_m;
    int num, den;
    int n;

    *n_channels = -1;

    /* resets the ss stream */
    if (ss_song != NULL) {
        free(ss_song);
        ss_song = NULL;
    }

    n_ss_ev = 0;

    /* sorts the song */
    song_sort();

    fpw = 0;
    frame = frame_ac = f_frame = 0;
    time_ac = time_ac_m = 0;
    num = den = 4;

    /* travels the song events generating softsynth song events */
    for (n = 0; n < n_song_ev; n++) {
        /* gets the song event */
        e = &song[n];

        /* calculates the frame */
        frame = ((e->time - time_ac) * fpw) + frame_ac;
        e->frame = frame;

        switch (e->type) {
        case SONG_EV_TEMPO:

            /* updates accumulations */
            frame_ac = frame;
            time_ac = e->time;

            /* calculates frames-per-whole based on new tempo */
            fpw = (double) ss_frequency *60.0;
            fpw /= e->amount / 4.0;

            add_ss_ev(e);

            break;

        case SONG_EV_METER:

            /* just store the values */
            num = e->min;
            den = e->max;
            time_ac_m = e->time;

            break;

        case SONG_EV_MEASURE:

            song_test_measure_boundary(e->time - time_ac_m,
                                       num, den, e->value);
            break;

        case SONG_EV_NOTE:

            /* assign a note id */
            e->note_id = note_id++;

            /* copy */
            add_ss_ev(e);

            /* copy again, but set as NOTE_OFF */
            e = add_ss_ev(e);
            e->type = SONG_EV_NOTE_OFF;

            frame += (int) (e->len * fpw);
            e->frame = frame;

            break;

        case SONG_EV_BACK:

            /* move the cursor back */

            frame_ac -= (int) (e->len * fpw);

            break;

        case SONG_EV_SS_PITCH_STRETCH:

            /* assign a note id */
            e->note_id = note_id++;

            /* copy */
            add_ss_ev(e);

            /* and copy again, as a note off */
            e = add_ss_ev(e);
            e->type = SONG_EV_NOTE_OFF;

            frame += (int) (e->len * fpw);
            e->frame = frame;

            break;

        case SONG_EV_SS_PRINT_WAVE_TEMPO:
        case SONG_EV_SS_WAV:
        case SONG_EV_SS_PAT:
        case SONG_EV_SS_SF2:
        case SONG_EV_SS_SUSTAIN:
        case SONG_EV_SS_ATTACK:
        case SONG_EV_SS_VIBRATO:
        case SONG_EV_SS_PORTAMENTO:
        case SONG_EV_SS_EFF_DELAY:
        case SONG_EV_SS_EFF_ECHO:
        case SONG_EV_SS_EFF_COMB:
        case SONG_EV_SS_EFF_ALLPASS:
        case SONG_EV_SS_EFF_FLANGER:
        case SONG_EV_SS_EFF_WOBBLE:
        case SONG_EV_SS_EFF_SQWOBBLE:
        case SONG_EV_SS_EFF_HFWOBBLE:
        case SONG_EV_SS_EFF_FADER:
        case SONG_EV_SS_EFF_REVERB:
        case SONG_EV_SS_EFF_FOLDBACK:
        case SONG_EV_SS_EFF_ATAN:
        case SONG_EV_SS_EFF_DISTORT:
        case SONG_EV_SS_EFF_OVERDRIVE:
        case SONG_EV_SS_EFF_OFF:
        case SONG_EV_SS_MASTER_VOLUME:
        case SONG_EV_SONG_INFO:
        case SONG_EV_EOT:

            /* just copy */
            add_ss_ev(e);
            break;

        case SONG_EV_SS_CHANNEL:

            /* count channels */
            if (*n_channels < e->channel)
                *n_channels = e->channel;

            add_ss_ev(e);
            break;

        case SONG_EV_NOP:
        case SONG_EV_MIDI_CHANNEL:
        case SONG_EV_MIDI_PROGRAM:

            /* ignored */
            break;

        case SONG_EV_NOTE_OFF:
        case SONG_EV_END:

            /* never found in generic song streams */
            break;
        }

        /* store the further frame seen */
        if (f_frame < frame)
            f_frame = frame;
    }

    /* generates an end of event mark, a time after the last one */
    e = add_event(&ss_song, &n_ss_ev);

    e->type = SONG_EV_END;
    e->frame = f_frame + ss_frequency;
    e->event_id = -1;

    /* finally sort */
    qsort(ss_song, n_ss_ev, sizeof(struct song_ev),
          frame_type_eventid_cmp);

    /* count one more */
    (*n_channels)++;
}


static const struct song_ev *process_this_frame_events(const struct song_ev
                                                       *e, int skip_frames)
/* process the events attached to this frame */
{
    static double tempo = 120.0;
    static int frame = 0;

    /* from the beginning? */
    if (e == NULL) {
        e = ss_song;
        tempo = 120.0;
        frame = 0;
    }

    if (verbose >= 1 && frame % ss_frequency == 0) {
        int m = frame / ss_frequency;
        printf("[%02d:%02d]\r", m / 60, m % 60);
        fflush(stdout);
    }

    while (e != NULL && e->frame == frame) {
        struct ss_ins *i;
        struct ss_wave *w;
        double freq;

        if (e->type == SONG_EV_NOTE ||
            e->type == SONG_EV_NOTE_OFF ||
            e->type == SONG_EV_SS_PITCH_STRETCH) {
            if (frame < skip_frames) {
                e++;
                frame = e->frame;
                continue;
            }
        }

        /* take the instrument */
        if (e->trk_id < 0)
            i = NULL;
        else
            i = &ss_song_ins[e->trk_id];

        switch (e->type) {
        case SONG_EV_NOTE:

            if (ss_ins_note_on(i, e->value, e->vol, e->note_id) < 0 &&
                verbose >= 1)
                printf("ss_ins_note_on error: track %d note %d\n",
                       e->trk_id, e->value);

            break;

        case SONG_EV_NOTE_OFF:

            ss_ins_note_off(i, e->note_id);

            break;

        case SONG_EV_SS_SUSTAIN:

            ss_ins_set_sustain(i, e->amount);

            break;

        case SONG_EV_SS_ATTACK:

            ss_ins_set_attack(i, e->amount);

            break;

        case SONG_EV_SS_VIBRATO:

            ss_ins_set_vibrato(i, e->depth, e->freq);

            break;

        case SONG_EV_SS_PORTAMENTO:

            ss_ins_set_portamento(i, (e->amount * 44100.0)
                                  / ((double) ss_frequency * 1000000.0));

            break;

        case SONG_EV_SS_CHANNEL:

            ss_ins_set_channel(i, e->channel, e->vol);

            break;

        case SONG_EV_SS_WAV:

            w = ss_load_wav_file(e->name,
                                 ss_note_frequency(e->value),
                                 ss_note_frequency(e->min),
                                 ss_note_frequency(e->max),
                                 e->start, e->end,
                                 e->channel, e->skip_channels);

            /* fail if can't open wav */
            if (w == NULL) {
                printf("Can't load wav '%s'\n", e->name);
                e = NULL;
            }
            else
                ss_ins_add_layer(i, w);

            break;

        case SONG_EV_SS_PAT:

            if (ss_load_pat_file(i, e->name) < 0) {
                printf("Can't load pat '%s'\n", e->name);
                e = NULL;
            }

            break;

        case SONG_EV_SS_SF2:

            if (ss_load_sf2_file(i, e->name, e->str2) < 0) {
                printf("Can't load instrument from sf2 '%s'\n", e->name);
                e = NULL;
            }

            break;

        case SONG_EV_SS_EFF_DELAY:

            ss_eff_delay(&i->effs[e->channel], e->len);
            break;

        case SONG_EV_SS_EFF_ECHO:

            ss_eff_echo(&i->effs[e->channel], e->len, e->vol);
            break;

        case SONG_EV_SS_EFF_COMB:

            ss_eff_comb(&i->effs[e->channel], e->len, e->vol);
            break;

        case SONG_EV_SS_EFF_ALLPASS:

            ss_eff_allpass(&i->effs[e->channel], e->len, e->vol);
            break;

        case SONG_EV_SS_EFF_FLANGER:

            ss_eff_flanger(&i->effs[e->channel],
                           e->len, e->vol, e->depth, e->freq, e->phase);
            break;

        case SONG_EV_SS_EFF_WOBBLE:

            ss_eff_wobble(&i->effs[e->channel], e->freq, e->phase, e->vol);

            break;

        case SONG_EV_SS_EFF_SQWOBBLE:

            ss_eff_square_wobble(&i->effs[e->channel], e->freq, e->phase);

            break;

        case SONG_EV_SS_EFF_HFWOBBLE:

            ss_eff_half_wobble(&i->effs[e->channel], e->freq, e->phase);

            break;

        case SONG_EV_SS_EFF_FADER:

            ss_eff_fader(&i->effs[e->channel], e->len, e->initial,
                         e->final);
            break;

        case SONG_EV_SS_EFF_REVERB:

            ss_eff_reverb(&i->effs[e->channel]);
            break;

        case SONG_EV_SS_EFF_FOLDBACK:

            ss_eff_foldback(&i->effs[e->channel], e->vol);
            break;

        case SONG_EV_SS_EFF_ATAN:

            ss_eff_atan(&i->effs[e->channel], e->vol);
            break;

        case SONG_EV_SS_EFF_DISTORT:

            ss_eff_distort(&i->effs[e->channel], e->vol);
            break;

        case SONG_EV_SS_EFF_OVERDRIVE:

            ss_eff_overdrive(&i->effs[e->channel], e->vol);
            break;

        case SONG_EV_SS_EFF_OFF:

            ss_eff_off(&i->effs[e->channel]);
            break;

        case SONG_EV_TEMPO:

            /* just store the last tempo */
            tempo = e->amount;
            break;

        case SONG_EV_SS_PITCH_STRETCH:

            /* find the wave */
            freq = ss_note_frequency(e->value);
            w = ss_ins_find_layer(i, freq, NULL);

            /* calculate optimal frequency */
            freq = ss_pitch_from_tempo(w, tempo, e->len);

            /* play the note */
            if (ss_ins_play(i, freq, e->vol,
                            e->note_id, w) < 0 && verbose >= 1)
                printf("ss_ins_play error: track %d freq %f\n",
                       e->trk_id, freq);

            break;

        case SONG_EV_SS_PRINT_WAVE_TEMPO:

            /* find the wave */
            freq = ss_note_frequency(e->value);
            w = ss_ins_find_layer(i, freq, NULL);

            /* print the optimal tempo */
            printf("Optimal tempo: %lf\n",
                   ss_tempo_from_wave(w, e->value, e->len));

            break;

        case SONG_EV_SS_MASTER_VOLUME:

            /* set master volume */
            ss_master_volume = e->vol;
            break;

        case SONG_EV_SONG_INFO:

            /* add a new song (track) */
            cue_file_song_info(frame, e->str2, e->name);
            break;

        case SONG_EV_EOT:

            /* end of track; trigger possible cleaning */
            ss_ins_disable(i);
            break;

        case SONG_EV_END:

            e = NULL;
            break;

        case SONG_EV_BACK:
        case SONG_EV_MIDI_CHANNEL:
        case SONG_EV_MIDI_PROGRAM:
        case SONG_EV_METER:
        case SONG_EV_MEASURE:
        case SONG_EV_NOP:

            /* never found in ss song streams */
            break;
        }

        /* next event */
        if (e)
            e++;
    }

    frame++;

    return e;
}


int ss_song_render(int skip_secs, const char *driver, const char *devfile)
{
    int n, i = 0;
    sample_t output[SS_MAX_CHANNELS];
    int skip_frames;
    int n_channels;
    const struct song_ev *e = NULL;

    /* convert the song to ss events */
    ss_song_convert_events(&n_channels);

    if (verbose >= 2)
        printf("Tracks: %d Channels: %d Events: %d\n",
               n_song_tracks, n_channels, n_ss_ev);

    if (trace) {
        printf("** SOFTWARE SYNTHESIZER EVENT DUMP **\n\n");
        dump_song_events(ss_song, n_ss_ev);
        return 0;
    }

    /* set the number of channels, unless forced */
    if (ss_nchannels == -1)
        ss_nchannels = n_channels > 0 ? n_channels : 2;

    if (ss_output_open(driver, devfile) < 0) {
        printf("Error: can't init driver\n");
        return 2;
    }

    /* init the generators */
    ss_gen_init();

    /* init the instruments */
    for (n = 0; n < n_song_tracks; n++)
        ss_ins_init(&ss_song_ins[n]);

    /* calculate the frame to start playing */
    skip_frames = skip_secs * ss_frequency;

    /* main loop */
    do {
        /* process all events in this frame */
        e = process_this_frame_events(e, skip_frames);

        /* reset frame samples */
        ss_output_init_frame(output);

        /* generate output from all instruments */
        for (n = i = 0; n < n_song_tracks; n++)
            i += ss_ins_frame(&ss_song_ins[n], output);

        /* dump to sampling driver */
        ss_output_write(output);
    } while (i);

    if (verbose >= 1)
        printf("\n");

    ss_output_close();

    return 0;
}
