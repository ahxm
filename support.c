/*

    Ann Hell Ex Machina - Music Software
    Copyright (C) 2003/2008 Angel Ortega <angel@triptico.com>

    support.c - Miscellaneous support functions

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "ahxm.h"
#include "sha1.h"


/** data **/

static char **library_path = NULL;
static int n_library_paths = 0;

static char located_file[2048];

/* global verbose flag */
int verbose = 1;

/* global tracing flag */
int trace = 0;

/* transparent converters */
struct transconv {
    const char *from;           /* extension from */
    const char *to;             /* extension to */
    const char *convcmd;        /* sprintf() format for converting command */
};

static struct transconv *transconvs = NULL;
static int n_transconvs = 0;


/** code **/

/**
 * libpath_add - Adds a directory path to the search path
 * @path: the directory path
 * @strip: flag to strip the upper level
 *
 * Adds @path to the list of searchable paths for libpath_fopen(),
 * optionally stripping the upper level if @strip is set.
 * The last part of the path is stripped before being stored,
 * and duplication is avoided.
 */
void libpath_add(const char *path, int strip)
{
    int n;
    char *ptr;
    char *p;

    /* if path starts with ~, set it to $HOME */
    if (*path == '~') {
        char *new;
        char *home;

        if ((home = getenv("HOME")) == NULL)
            return;

        if ((new = malloc(strlen(home) + strlen(path) + 2)) == NULL)
            return;

        strcpy(new, home);
        strcat(new, "/");
        strcat(new, path + 1);

        p = new;
    }
    else {
        /* just duplicate */
        p = strdup(path);
    }

    /* if no directory path remains, abort */
    if ((ptr = strrchr(p, '/')) == NULL) {
        free(p);
        return;
    }

    /* strip the filename part */
    if (strip)
        *ptr = '\0';

    /* now try to find if that path is already stored */
    for (n = 0; n < n_library_paths; n++) {
        if (strcmp(p, library_path[n]) == 0) {
            /* found; free and return */
            free(p);
            return;
        }
    }

    /* add room for the new path */
    n_library_paths++;
    library_path =
        (char **) realloc(library_path, n_library_paths * sizeof(char *));

    /* store */
    library_path[n_library_paths - 1] = p;
}


/**
 * libpath_fopen - Opens a file, optionally searching in a path list
 * @filename: the file name
 * @mode: the file mode
 *
 * Opens a file. If the file is found as is, it's opened;
 * otherwise, the full list of directory paths maintained by
 * libpath_add() is searched until it's found or the
 * end of the list is reached. Whenever a file
 * is successfully opened, its path is also stored.
 */
FILE *libpath_fopen(const char *filename, const char *mode)
{
    int n;
    FILE *f = NULL;

    /* try first here */
    if ((f = fopen(filename, mode)) != NULL) {
        strncpy(located_file, filename, sizeof(located_file));
        located_file[sizeof(located_file) - 1] = '\0';

        libpath_add(filename, 1);
        return f;
    }

    /* couldn't open; try concatenating all stored paths */
    for (n = n_library_paths - 1; n >= 0; n--) {
        snprintf(located_file, sizeof(located_file), "%s/%s",
                 library_path[n], filename);
        located_file[sizeof(located_file) - 1] = '\0';

        if ((f = fopen(located_file, mode)) != NULL) {
            libpath_add(located_file, 1);
            break;
        }
    }

    return f;
}


/**
 * libpath_locate - Locates a file inside the path
 * @filename: the file to be located
 *
 * Locates a file inside the library path maintained by libpath_fopen()
 * and add_library_path(). If the file is found, a pointer to a static
 * buffer containing the real path of the file is returned, or
 * NULL otherwise.
 */
char *libpath_locate(const char *filename)
{
    FILE *f;

    if ((f = libpath_fopen(filename, "r")) == NULL)
        return NULL;

    fclose(f);
    return located_file;
}


/**
 * libpath_print - Prints the library path
 *
 * Prints the library path.
 */
void libpath_print(void)
{
    int n;

    for (n = 0; n < n_library_paths; n++) {
        if (n)
            printf(";");

        printf("%s", library_path[n]);
    }
}

/** transparent conversions **/

void transconv_add(const char *from, const char *to, const char *convcmd)
/* adds a converter */
{
    struct transconv *t;

    GROW(transconvs, n_transconvs, struct transconv);

    t = &transconvs[n_transconvs++];

    t->from = strdup(from);
    t->to = strdup(to);
    t->convcmd = strdup(convcmd);
}


static char *transconv_sha_file(const char *file, const char *ext)
/* builds a unique cache file basename using a SHA1 hash */
{
    static char c_file[64];
    unsigned char sha1[20];
    SHA_CTX c;
    int n;

    SHA1_Init(&c);
    SHA1_Update(&c, (char *) file, strlen(file));
    SHA1_Update(&c, (char *) ext, strlen(ext));
    SHA1_Final(sha1, &c);

    for (n = 0; n < sizeof(sha1); n++) {
        char tmp[3];

        snprintf(tmp, sizeof(tmp), "%02x", sha1[n]);
        c_file[n * 2] = tmp[0];
        c_file[(n * 2) + 1] = tmp[1];
    }

    c_file[n * 2] = '\0';
    return c_file;
}


static char *transconv_unique_file(const char *file, const char *ext,
                                   const char *dir)
/* builds a unique cache file name with complete path */
{
    static char tmp[2048];
    char *c_path = NULL;

    if ((c_path = getenv("TEMP")) == NULL)
        if ((c_path = getenv("TMP")) == NULL)
            c_path = "/tmp";

    /* build the directory cache name */
    snprintf(tmp, sizeof(tmp), "%s/%s-%d", c_path, dir, getuid());
    tmp[sizeof(tmp) - 1] = '\0';

    /* create the cache directory */
#if CONFOPT_MKDIR_ARGS == 2
    mkdir(tmp, 0755);
#else
    mkdir(tmp);
#endif

    strcat(tmp, "/");
    strcat(tmp, transconv_sha_file(file, ext));
    strcat(tmp, ext);

    return tmp;
}


char *transconv_pipe(const char *cmd, const char *ext, const char *dir)
/* executes cmd as a pipe */
{
    char *c_file = transconv_unique_file(cmd, ext, dir);

    /* does the file already exist? */
    if (access(c_file, R_OK)) {
        char tmp[2048];

        snprintf(tmp, sizeof(tmp), cmd, c_file);
        tmp[sizeof(tmp) - 1] = '\0';

        if (verbose >= 2)
            printf("Converting: %s\n", tmp);

        system(tmp);
    }

    return c_file;
}


char *transconv(const char *file, const char *ext, const char *dir)
/* converts using the transparent converters and the cache, if needed */
{
    char *this_ext;
    char *c_file;
    int n;
    struct transconv *t;

    /* gets this file extension */
    if (file == NULL || (this_ext = strrchr(file, '.')) == NULL)
        return NULL;

    /* if it's already the desired type, do nothing */
    if (strcmp(ext, this_ext) == 0)
        return (char *) file;

    /* get a unique name */
    c_file = transconv_unique_file(file, ext, dir);

    /* does the file already exist? */
    if (access(c_file, R_OK) == 0)
        return c_file;

    /* no; look for a suitable converter */
    for (n = 0, t = transconvs; n < n_transconvs; n++, t++) {
        if (strcmp(t->from, ".*") == 0 ||
            (strcmp(ext, t->to) == 0 && strcmp(this_ext, t->from) == 0)) {
            char tmp[2048];

            /* found a converter! just do it */
            snprintf(tmp, sizeof(tmp), t->convcmd, c_file, file);
            tmp[sizeof(tmp) - 1] = '\0';

            if (verbose >= 2)
                printf("Executing: %s\n", tmp);

            if (system(tmp) == 0)
                break;
            else
                unlink(c_file);
        }
    }

    return c_file;
}
